#!python
#cython: language_level=3


from libc.stdlib cimport malloc, free 
from libc.string cimport strcpy


cdef extern from "bmrio.h": 
    ctypedef void (*bmrioBBp)(double *xk, int n, double *fk, double *gk, int *indic, void *context )
    cdef int BMRIOmin(double *x0, int n, bmrioBBp oracle_fn, void * BBContext,  int memax, int imp, int impFile, int mt, double errabs, double *lb, double *ub, double *x_last, double *f_last, double *x_serious, double *f_serious, double *time, int maxBBcall, double relTol)


## global elements
cdef object py_bb= None #This for the bb function

import numpy as np
import os

# bb
cdef void myBMRIObb(double *x, int n, double *fk, double *gk, int *indic, void *context): 
    cdef int i
    cdef int status
    xx = np.zeros(n)
    for i in range(n):
        xx[i] = x[i]
    bbMode = 2 # always is computed function value and subgradient
    if  py_bb != None: 
        bbVal, bbSg, status = py_bb(xx, mode=bbMode)
        fk[0] = bbVal
        for i in range(n): 
            gk[i] = bbSg[i]
        if status != 0:
            indic[0] = status
    else:
        print('Null pointer in bb function!!')
        indic[0] = -1

def minimize(x0, fun=None, grad=None, bb=None, bbContext=None, 
        bundleMaxSize=2,
        lb=None,
        ub=None,
        outputMessageLevel = 4,
        bmrioAlg = 1,
        inexactError=0,
        globalLog=None,
        tol=1e-5, maxIter=10):

    import pyminit.tools
    import pyminit.common as common


    cdef int mt=bmrioAlg
    cdef int memax=bundleMaxSize
    cdef int imp
    cdef int impFile
    cdef double errabs=inexactError


    cdef bmrioBBp cBB = NULL
    cdef void *cBBContext = NULL
    cdef double ctol=tol
    cdef i
    cdef int status
    
    cdef double *cLb=NULL
    cdef double *cUb=NULL
    cdef double[:] vLb
    cdef double[:] vUb

    cdef bytes py_bytes1, py_bytes2

    ## log
    if globalLog is not None: 
        imp = outputMessageLevel
        impFile = 1
    else:
        imp =  0
        impFile = 0


    ## initial point

    x0 = np.array(x0)
    cdef int n = len(x0)
    cdef double[:] vx0 = x0

    ## bounds
    if lb is not None:
        pyLb = np.ones(n)
        if isinstance(lb, (list, np.ndarray)): 
            pyLb[:] = np.array(lb)
        else:
            pyLb = lb * pyLb
        vLb = pyLb
        cLb = &vLb[0]
            
    if ub is not None:
        pyUb = np.ones(n)
        if isinstance(ub, (list, np.ndarray)): 
            pyUb[:] = np.array(ub)
        else:
            pyUb = ub * pyUb
        vUb = pyUb
        cUb = &vUb[0]

    ## bb
    if bb is None:
        if ((fun is None) or (grad is None)):
            print("Error: ('fun' and 'grad') or 'bb' must be provided")
            return common.result_class(status=1)
        else:
            bb = lambda x, mode=0, context=None: pyminit.tools.fgBB(x, mode=mode, context=context, fun=fun, grad=grad)

    global py_bb
    py_bb = lambda x, mode=0: bb(x, mode=mode, context=bbContext)
    cBB = &myBMRIObb

    ## Output
    cdef int nIter = -1
    x_best = np.zeros(n)
    cdef double [:]vx_best = x_best
    x_last = np.zeros(n)
    cdef double [:]vx_last = x_last
    cdef double f_best = 0.0
    cdef double f_last = 0.0
    cdef double time = 0.0
    cdef double deltak = 0.0

    status = BMRIOmin(&vx0[0], n, cBB,  cBBContext,  memax, imp, impFile, mt, errabs, cLb, cUb, &vx_last[0], &f_last, &vx_best[0], &f_best, &time,maxIter, tol)

    ## log
    if globalLog is not None: 
        fortranLogFile = './fort.88'

        if os.path.isfile(fortranLogFile):
            os.rename(fortranLogFile, globalLog)
        else:
            print('No log file was generated')

    return x_best, f_best, x_last, f_last, deltak, nIter, time, status
