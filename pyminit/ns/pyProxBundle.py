import numpy as np
import pyminit.common as common
import time

#stop criterion
def stopCriterionDefault(sa, ea, muk, deltak, k, tol):
    if k == 0:
        deltak = 100
    return deltak <= tol

def stopCriterionInfNorm(sa, ea, muk, deltak, k, tol):
    if k == 0:
        deltaAlternative = 100
    else:
        deltaAlternative = ea + max(np.abs(sa))
    return deltaAlternative <= tol

# prox-param

def proxParamDefault(xk, sk, muk, zkprox, skprox, mu, iterK, changeProxCenter, context=None):
    if changeProxCenter:
        dx = zkprox - xk
        ds = skprox - sk
        dsds = np.dot(ds,ds)
        if dsds < 1e-5:
            extra = 0
        else:
            extra = np.dot(dx, ds)/dsds
        aux = (1.0/muk) + extra
        # print('ds = {}'.format(ds)) #jp: debugging
        # print('muk = {}'.format(muk)) #jp: debugging
        # print('1/mukp = {}'.format(aux)) #jp: debugging
        newMu = 1.0/aux
    else:
        newMu = mu + 1
    return newMu

# bundle compression
def bundleCompressProxLMDual(bundle, context={}):

    if 'maxSize' in context: 
        maxSize = context['maxSize']
    else:
        maxSize = 0

    if 'type' in context: 
        tt = context['type']
    else:
        tt = 'active'


    if tt == 'flush':
        return []
    elif tt == 'active':
        if 'alpha' in context:
            # bundle = [ b for b, a in zip(bundle, context['alpha']) if a>0]
            aux = {b_id: context['alpha'][b_id] for b_id in context['alpha'] if context['alpha'][b_id]> 1e-4}
            alphaSum = sum([aux[b_id] for b_id in aux])
            context['alpha'] =  {b_id: aux[b_id]/alphaSum for b_id in aux}
            bundle = [ b for b in bundle if (b[0] in context['alpha'])]
        if maxSize > 0:
            size = min(maxSize, len(bundle))
            bundle = bundle[:size]
        return bundle
        
    return bundle


#solving the cutting plane model
def solveProxLMDual(xk, fk, bundle, muk, 
        bundleManagement=None, bundleManagementContext=None):

    if bundleManagement is None:
        bundleManagement = bundleCompressProxLMDual

    nb = len(bundle)
    n = len(xk)

    sa = np.zeros(n)
    ea = 0

    option = 'gurobi'

    if option == 'gurobi':
        import gurobipy as grb


        bIndex = list(range(nb))
        m = grb.Model('dual')
        m.setParam('LogtoConsole', 0)
        alpha = m.addVars(bIndex, vtype=grb.GRB.CONTINUOUS, lb=0.0, ub=1.0, name='alpha')
        m.update()

        # Constrain sum alpha ==1
        m.addConstr(grb.quicksum(alpha) == 1, name='sum1')

        # Setting the objective function
        # print(bundle) # JP:  debbuging
        objF = grb.quicksum(b[-1] * alpha[i] for i, (id_b, b) in enumerate(bundle))
        objF += grb.quicksum(np.dot(bundle[i][1][0], bundle[j][1][0]) * alpha[i] * alpha[j] for i in bIndex for j in bIndex) / (2 * muk)
        m.setObjective(objF, sense=grb.GRB.MINIMIZE)

        # Solving the problem
        # m.write('jj.lp')
        # m.write('jj.mps')
        m.optimize()
        alphak = np.zeros(nb)
        optVal = m.objVal

        for i, (id_b, b) in enumerate(bundle):
            sa += alpha[i].x * b[0]
            ea += alpha[i].x * b[-1]
            # alphak[i] = alpha[i].x
        alphak = {id_b: alpha[i].x for i, (id_b, b) in enumerate(bundle)}
    else:
        import scipy.optimize as sopt

        def objFun(alpha):
            val = 0
            vecVal = np.zeros(len(bundle[0][0]))
            for a, b in zip(alpha, bundle):
                val += a * b[-1]
                vecVal += a * b[0]
            val += np.dot(vecVal, vecVal) / (2 * muk) 
            return val
        cons = [{'type': 'eq',
            'fun': lambda alpha: sum(alpha) -1,
            'jac': lambda alpha: np.ones(nb)}]
        
        aux = np.eye(nb)
        for i in range(nb):
            cons.append({'type': 'ineq',
                'fun': lambda alpha, i=i: alpha[i] ,
                'jac': lambda alpha, i=i:  aux[i]})

        res = sopt.minimize(objFun, aux[0],  jac=None, constraints=cons, method='SLSQP', options={'disp': True})
        alphak = res.x
        optVal = res.fun
        for a, (id_b, b) in zip(alphak, bundle):
            ea += a * b[-1]
            sa += a * b[0]


    ea = max([0, ea]) # Just for avoiding negative values of ea

    zkprox = xk - (sa/ muk)
    proxOptVal = fk - optVal
    deltak = optVal
    stat = 0
    # compressing the bundle
    bundleManagementContext.update({'alpha': alphak})
    bundle = bundleManagement(bundle, context=bundleManagementContext)
    return sa, ea, zkprox, proxOptVal, deltak, bundle, stat
        

def proxBundle(x0, bb, bbContext=None, 
        solveProxM=None, solveProxMConxtext=None,
        proxParam=None, proxParamContext=None,
        bundleManagement=None, bundleManagementContext=None,
        bundle=[],
        mu0=1,
        m=0.5,
        context=None, 
        xLog=None, globalLog=None, 
        stopCriterion=None,
        tol=1e-5, maxIter=10): 
    '''
    def proxBundle(bb, x0, context=None, mu0=1, m=0.5, 
        solveProxLM=None, 
        nullStepParamUpdate=None,
        seriousStepParamUpdate=None,
        bundleCompress=None, bundleCompressContex={},
        xLog=None, globalLog=None, 
        tol=1e-5, maxIter=10): 
 
    bb: non smooth black box
    x0: initial point
    xLog: path file for recording the sequence rengerated by the algorithm
    globalLog: path file for general information 
    maxIter:
    '''

    startTime = time.time()
    if stopCriterion is None:
        stopCriterion = stopCriterionDefault
    if xLog is not None: xLogFile = open(xLog, 'w')
    if globalLog is not None: globalLogFile = open(globalLog, 'w')
    if proxParam is None:
        proxParam = proxParamDefault
    if context is None:
        context = {}

    if solveProxM is None:
        solveProxM = solveProxLMDual
    if bundleManagement is None:
        bundleManagement = bundleCompressProxLMDual
        if bundleManagementContext is None:
            bundleManagementContext = {}
    xk = np.array(x0) #prox center
    newBundleId = 0
    if bbContext is not None:
        if 'cutId' in bbContext:
            bbContext['cutId'] = newBundleId
    fk, sk, status = bb(xk, mode=2, context=bbContext) # sk: proximal center subgrad

    bundle.append((newBundleId, (sk, 0))) # defining the initial bundle
    muk = mu0
    mu = muk

    iterK = 0
    seriousK = 0
    nullLog = ''
    sa = [] #just for using the fist time on stopCriterion
    ea = 100 #just for using the fist time on stopCriterion
    deltak= tol  + 1 # ensuring the first iteration

    if xLog is not None: 
        xLogFile.write(','.join(['[k={}][serious={}{}]'.format(iterK, seriousK, nullLog)] + [str(val) for val in xk]) + '\n')


    if globalLog is not None: 
        globalLogFile.write('[k={}],[serious={}{}],f(xk)={},f(z k+1)={},deltak={},proxOptVal={}, mu={},bundleSize={}\n'.format(
            iterK, seriousK, nullLog,fk, None, deltak, None, None, None))
    
    stopOK = stopCriterion(sa, ea, mu0, deltak, iterK, tol)
    while ((iterK < maxIter) and not stopOK):
        iterK +=1
        bundleSize = len(bundle)

        newBundleId += 1
        if bundleManagementContext is not None:
            if 'cutId' in bundleManagementContext:
                bundleManagementContext['cutId'] = newBundleId
        sa, ea, zkprox, proxOptVal, deltak, bundle, pmStatus = solveProxM(
                xk, fk, bundle, mu, 
                bundleManagement=bundleManagement, bundleManagementContext=bundleManagementContext)

        bundle.append((newBundleId, (sa, ea)))

        newBundleId += 1 #newBundleId(1)
        if bbContext is not None:
            if 'cutId' in bbContext: 
                bbContext['cutId'] = newBundleId
        fzkprox, skprox, bbstatus = bb(zkprox, mode=2, context=bbContext)

        # Checking if a serious step was reached
        if  fzkprox < fk - m * deltak: # we have a serious step
            nullLog =  ''
            changeProxCenter = True
            muk = proxParam(xk, sk, muk, zkprox, skprox, mu, iterK, changeProxCenter=changeProxCenter, context=proxParamContext)
            
            # Changing the center of the bundle
            bundle = [(id_b, (s, fzkprox - (fk + np.dot(s, zkprox - xk) - e)))  for id_b, (s, e) in bundle] 
            bundle.append((newBundleId, (skprox, 0))) #This newBundleId comes from newBundleId(1)

            # Updating the prox center 
            xk = zkprox
            fk = fzkprox
            
            seriousK = iterK
            mu = muk
        else: # null step
            nullLog = '-null'
            changeProxCenter = False
            ekprox = fk - (fzkprox + np.dot(skprox, xk - zkprox))
            bundle.append((newBundleId, (skprox, max([0, ekprox]))))#This newBundleId comes from newBundleId(1)
            mu = proxParam(xk, sk, muk, zkprox, skprox, mu, iterK, changeProxCenter=changeProxCenter, context=proxParamContext)

        if xLog is not None: 
            xLogFile.write( ','.join(['[k={}][serious={}{}]'.format(iterK, seriousK, nullLog)] 
                + [str(val) for val in zkprox]) + '\n')
        if globalLog is not None: 
            globalLogFile.write('[k={}],[serious={}{}],f(xk)={},f(z k+1)={},deltak={},proxOptVal={}, mu={},bundleSize={}\n'.format(
                iterK, seriousK, nullLog,fk, fzkprox, deltak, proxOptVal, mu, bundleSize))

        stopOK = stopCriterion(sa, ea, mu0, deltak, iterK, tol)

    if xLog is not None: xLogFile.close()
    if globalLog is not None: globalLogFile.close()
    endTime = time.time()
    
    if not stopOK:
        status = 0
    elif iterK>=maxIter:
        status = 10
    else:
        status = bbstatus

    return common.result_class(x_best=xk, f_best=fk, x_last=zkprox, f_last=fzkprox, deltak=deltak, nIter=iterK, status=status, time=endTime-startTime)


def minimize(x0, fun=None, grad=None, bb=None, bbContext=None, 
        solveProxM=None, solveProxMConxtext=None,
        proxParam=None, proxParamContext=None,
        bundleManagement=None, bundleManagementContext=None,
        bundleManagerFun=None, bundleManagerContext=None,
        bundle=None,
        mu0=1,
        m=0.5,
        context=None, 
        xLog=None, globalLog=None, 
        stopCriterion=None,
        tol=1e-5, maxIter=10): 

    import pyminit.tools

    bundleManagement=bundleManagerFun
    bundleManagementContext=bundleManagerContext

    if bb is None:
        if ((fun is None) or (grad is None)):
            print("Error: ('fun' and 'grad') or 'bb' must be provided")
            return common.result_class(status=1)
        else:
            bb = lambda x, mode=0, context=None: pyminit.tools.fgBB(x, mode=mode, context=context, fun=fun, grad=grad)
    if bundle is None:
        bundle = []

    ## prox solver 
    if solveProxM is not None:
        if isinstance(solveProxM, str): 
            print('Using the dual solver. Currently it is the only option ')
            solveProxM = solveProxLMDual
    else: 
        solveProxM = solveProxLMDual



    result = proxBundle(x0, bb, bbContext=bbContext, solveProxM=solveProxM, solveProxMConxtext=solveProxMConxtext, proxParam=proxParam, proxParamContext=proxParamContext, bundleManagement=bundleManagement, bundleManagementContext=bundleManagementContext, bundle=bundle, mu0=mu0, m=m, context=context, xLog=xLog, globalLog=globalLog, stopCriterion=stopCriterion, tol=tol, maxIter=maxIter)
    return result
