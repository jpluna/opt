## Proximal Bundle Method
We use the cutting plane method for solving 

$`\begin{array}{rl}
\text{min}& f(x)\\
\text{s.t.} &  x\in X\\
\end{array}`$

where the function $`f:\mathbb{R}^n\to\mathbb{R}`$,   is convex  not necessarily differentiable.
The feasible set  $`X`$  is any **closed convex set**(maybe unbounded). 

```python
import numpy as np
import pyminit.ns.proxBundle as pb
def f(x):
	return np.abs(x).sum() - 1

def s(x):
	sg = np.ones_like(x)
	sg[x<0] = -1.0
	return sg
x0 = np.ones(2)
result = pb.minimize(x0, fun=f, grad=s)
print(result)
```
```python
[0. 0.] -1.0 0
```
### Using a `black-box`
```python
def bb(x, mode=0, context=None):
	status = 0
	if mode == 0: 
		return np.abs(x).sum() - 1, status
	elif mode == 1:
		sg = np.ones_like(x)
		sg[x<0] = -1.0
		return sg, status
	elif mode == 2:
		sg = np.ones_like(x)
		sg[x<0] = -1.0
		return np.abs(x).sum() - 1, sg, status

x0 = np.ones(2)
result = pb.minimize(x0, bb=bb)
print(result)
```
