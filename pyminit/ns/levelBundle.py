import numpy as np
import pyminit.common as common
import time

def H(Fx, L, m):
    h = 0
    if m > 0:
        aux = np.array(Fx)
        aux[0] = aux[0] -  L
        h = max(aux)
    else:
        h = Fx - L
    return h

def continueAlg(subroutineName, status):
    OK = status == 0
    if not OK:
        if subroutineName == 'bb': 
            print('Black Box ended with status {}'.format(status))
    return OK


def appSolverDefault2(x_best, f_best, xk, fk, levK, bundle, context=None):
    import gurobipy as grb

    n = context['n']
    m = context['m']
    nb = len(bundle)

    xIndex = tuple(range(n))
    # cutIndex = tuple((b, j) for b in range(nb) for j in  range(m + 1))

    if 'model' not in context:
        lb = context['lb']
        ub = context['ub']
        if lb is None:
            lb = (-1e5) *  np.ones(n)

        if ub is None:
            ub = (1e5) *  np.ones(n)
        
        model = grb.Model('level')
        model.setParam('LogtoConsole', 0)
        # Setting variables
        x = model.addVars(xIndex, vtype=grb.GRB.CONTINUOUS, lb=lb, ub=ub, name='x')
        model.update()

        # Objective function
        model.setObjective(0 , grb.GRB.MINIMIZE)
        context['cutsId']=[]
        context['model'] = model
        context['model-x'] = x

    else:
        model = context['model']
        # x = {int(v.varName.split('[')[-1].split(']')[0]): v for v in model.getVars()}
        x = context['model-x']

    # bundleIds = [idc for idc, c in bundle]
    # newCuts = [b for b in bundle if b[0] not in context['cutsId']]
    # idCutsToBeRemoved = [str(idc) for idc in context['cutsId'] if idc not in bundleIds] # ids are transform to str because constraints names in guroby are str
    # context['cutsId'] = bundleIds

    # Setting constraints
    # removing cuts
    # constrs = model.getConstrs()
    # for c in constrs:
        # if c.constrName.split('-')[0] in idCutsToBeRemoved:
            # model.remove(c)
    # adding new cuts
    for b_id, (xk, Fxk, Gxk) in bundle:
        if b_id not in context['cutsId']:
            if m > 0:
                model.addConstrs((Fxk[0] - np.dot(Gxk[0], xk) + grb.quicksum(Gxk[0][l] * x[l] for l in xIndex) <= levK), "{}-0".format(b_id))
                for j in range(1,m + 1):
                        model.addConstr((Fxk[j] - np.dot(Gxk[j], xk) + grb.quicksum(Gxk[0][l] * x[l] for l in xIndex) <= 0), "{}-{}".format(b_id, j))
            else: 
                model.addConstr((Fxk - np.dot(Gxk, xk) + grb.quicksum(Gxk[l] * x[l] for l in xIndex) <= levK), "{}".format(b_id))
            context['cutsId'].append(b_id)

    # Solving the problem
    model.optimize()
    xk = np.zeros(n)
    if model.status == grb.GRB.Status.INFEASIBLE:
        status = 1
    else:
        for i in xIndex:
            xk[i] = x[i].X
        status = 0
    # grb.disposeDefaultEnv()
    return xk, status

def appSolverDefault(x_best, f_best, xk, fk, levK, bundle, context=None):
    import gurobipy as grb

    n = context['n']
    m = context['m']
    lb = context['lb']
    ub = context['ub']
    if lb is None:
        lb = (-1e5) *  np.ones(n)

    if ub is None:
        ub = (1e5) *  np.ones(n)
    
    model = grb.Model('level')
    model.setParam('LogtoConsole', 0)

    nb = len(bundle)

    # Setting variables

    xIndex = tuple(range(n))
    # cutIndex = tuple((b, j) for b in range(nb) for j in  range(m + 1))
    x = model.addVars(xIndex, vtype=grb.GRB.CONTINUOUS, lb=lb, ub=ub, name='x')
    # t = m.addVar(vtype=grb.GRB.CONTINUOUS, name='t')

    # Setting constraints
    # Cutting planes
    if m > 0:
        for b_id, (xk, Fxk, Gxk)  in bundle: 
            model.addConstrs((Fxk[0] - np.dot(Gxk[0], xk) + grb.quicksum(Gxk[0][l] * x[l] for l in xIndex) <= levK), "{}-0".format(b_id))
            for j in range(1,m + 1):
                model.addConstr((Fxk[j] - np.dot(Gxk[j], xk) + grb.quicksum(Gxk[0][l] * x[l] for l in xIndex) <= 0), "{}-{}".format(b_id, j))
    else:
        for b_id, (xk, Fxk, Gxk) in bundle:
            model.addConstr((Fxk - np.dot(Gxk, xk) + grb.quicksum(Gxk[l] * x[l] for l in xIndex) <= levK), "{}".format(b_id))

    # Objective function
    model.setObjective(0 , grb.GRB.MINIMIZE)

    # Solving the problem
    model.optimize()
    xk = np.zeros(n)
    if model.status == grb.GRB.Status.INFEASIBLE:
        status = 1
    else:
        for i in xIndex:
            xk[i] = x[i].X
        status = 0
    # grb.disposeDefaultEnv()
    return xk, status

def levelBundle(x0, L0,
        bb, bbContext=None,
        appSolver=None, appSolverContext=None, 
        bundle=[],
        lb=None, ub=None,
        gamma=0.5,
        xLog=None, globalLog=None, 
        tol=1e-5, maxIter=10): 
    '''

levelCutting(x0, L0, bb,  bbContext=None, appSolver=None, appSolverContext=None bundle=[],  lb=None, ub=None, gamma=0.5, xLog=None, globalLog=None, tol=1e-5, maxIter=10)

Parameters
----------
x0 : numpy array. 
	It represents the initial point at wich the algorithm will start.
L0: float
	A lower bound for the minimization problem
bb: Black Box funtion for `f=(f_1, f_2,..)`, where `f_1` is the objective function and `f_j`, j>1 are the explict constraints
bbContext: any object for probiding information to the bb.
appSolver: function. If None, the algorithm will assume there no explict constraints and will use an internal function. 

        appSolverDefault(x_best, f_best, xk, fk, levK, bundle, context=None)
		xk : numpy array
			Initial point  for the function
		levK: float
				Level value
		bundle: list [(x1, F1, G_1),(x2, F2, G2)... ]: 
					xi: numpy array
					Fi: float of array of same nature of `f(x)`
					Gi: 1D or 2D array  of same shape as  the jacobian of `f(x)`
    	return xkp, status
	The function finds and returns a point `xkp` (and a status =0) such that 
		- `xkp` belong to `X`
		- Fi + np.dot(G1, xkp -xi) <= [levK, 0,0..] for each (xi, Fi, Gi) in the bundle.
	in case such point does not exists, the function returns status=1
ub,lb: numpy arrays
	upper and lower bound for the feasible set. If appSovler is provided, ub and lb are negleted.
globalLog: string
	path for log file

Returns
-------
xk : numpy array
	Minimizer 
Fxk : float
	Optimal value
status : int
	0: algorithm ended suscessfully

    '''

    startTime = time.time()
    status = 0

    F, G, bbStatus = bb(x0, mode=2, context=bbContext)
    bbType = G.shape
    if len(bbType) > 1: #bb with  constraints
        m, n = bbType
    else: #bb without constraints
        n = bbType[0]
        m = 0
    if m == 0: 
        fval = F
    else:
        fval = F[0]

    if appSolver is None:
        appSolver = appSolverDefault
        appSolverContext = {'lb': lb, 'ub': ub, 'n':n, 'm':m}

    if  not continueAlg('bb', bbStatus):
        status = 1 
        return common.result_class(status=status)

    newBundleId = 0
    bundle.append((newBundleId, (x0, F, G)))
    h0 = H(F, L0, m)
    Pk = [(x0, F, h0)]
    xk = x0
    iterK = 0

    if globalLog is not None: gFile = open(globalLog, 'w')
    if xLog is not None: xLogFile = open(xLog, 'w')

    if globalLog is not None: gFile.write('Decision variable length: {}\n'.format(len(x0)))
    if globalLog is not None: gFile.write('gamma: {}\n'.format(gamma))
    if globalLog is not None: gFile.write('[iter {}], f_best={}, f_last={}, LevK={}, L={}, h0={}, hk={}, bundleSize={}\n'.format(iterK, fval, fval, None, L0, h0, None, len(bundle)-1))
    if xLog is not None: xLogFile.write(','.join(['[k={}]'.format(iterK)] + [str(val) for val in xk]) + '\n')

    if h0 < 0:
        print('Error. Information incompatible. Initial point nonfeasible or lower bound supplied is actually not a lower bound')
        Fxk = Pk[0][1]
        status = 2
        if globalLog is not None: 
            gFile.write('Error. Information incompatible. Initial point nonfeasible or lower bound supplied is actually not a lower bound\n')
            gFile.write('status = {}\n'.format(status))
            gFile.close()
        return common.result_class(status=status)
    x_best = np.array(x0)
    x_last = x_best
    f_best = fval
    f_last = f_best
    while ((iterK < maxIter) and (h0 > tol)):
        hk = h0
        while ((hk> (1-gamma) * h0) and (iterK < maxIter)):
            # print(bundle)
            iterK += 1
            levK = L0 + gamma * hk
            xkp, levStatus = appSolver(x_best, f_best, x_last, f_last, levK, bundle, context=appSolverContext)

            if levStatus == 0: # level set  is non empty
                if xLog is not None: xLogFile.write(','.join(['[k={}]'.format(iterK)] + [str(val) for val in xkp]) + '\n')
                # print(xkp)
                F, G, bbStatus = bb(xkp, mode=2, context=bbContext)
                if  not continueAlg('bb', bbStatus):
                    status = 1 
                    return common.result_class(status=status)
                newBundleId += 1
                bundle.append((newBundleId, (xkp, F, G)))
                f_last = F if m == 0 else F[0]
                x_last = xkp
                Pk.append((xkp, F, -1)) # -1 is the error that at this point is irrelevant
            else: # level set empty (level too low)
                if globalLog is not None: gFile.write('Level set empty\n') 
                L0 = levK
            # Finding the best point
            Pk = [(x, F, H(F, L0, m)) for x, F, h in Pk]
            xk, Fxk, hk = min(Pk, key=lambda x:x[-1])
            

            if hk < 0:
                print('Error. Information incompatible. Initial point nonfeasible or lower bound supplied is actually not a lower bound')
                status = 2
                if globalLog is not None: 
                    gFile.write('Error. Information incompatible. Initial point nonfeasible or lower bound supplied is actually not a lower bound\n')
                    gFile.write('status = {}\n'.format(status))
                    gFile.close()
                return common.result_class(status=status)

            x_best = xk
            if m == 0: 
                fval = Fxk  
            else:
                fval = Fxk[0]

            f_best = fval

            if globalLog is not None: gFile.write('[iter {}], f_best={}, f_last={}, LevK={}, L={}, h0={}, hk={}, bundleSize={}\n'.format(iterK, f_best, f_last, levK, L0, h0, hk, len(bundle)-1))
        h0 = hk
        if globalLog is not None: gFile.write('Bundle managed\n')

    if ((iterK >= maxIter) and (h0 > tol)): 
        print('Maximum of iterations reached without satisfiying of tolerance error')
        status = 1
        if globalLog is not None: 
            gFile.write('Maximum of iterations reached without satisfiying tolerance error\n')
            gFile.write('status = {}\n'.format(status))
    if globalLog is not None: gFile.close()
    endTime = time.time()
    return common.result_class(x_best=x_best, f_best=f_best, x_last=x_last, f_last=f_last, status=status, deltak=hk, nIter=iterK, time=endTime-startTime)

def minimize(x0, fun=None, grad=None, bb=None, bbContext=None,
        appSolver=None, appSolverContext=None, 
        bundle=[],
        L0=None,
        lb=None, ub=None,
        gamma=0.5,
        xLog=None, globalLog=None, 
        tol=1e-5, maxIter=10):
    '''
    minimize(x0, fun=None, grad=None, bb=None, bbContext=None,
        appSolver=None, appSolverContext=None, 
        bundle=[],
        L0=None,
        lb=None, ub=None,
        gamma=0.5,
        xLog=None, globalLog=None, 
        tol=1e-5, maxIter=10):
    '''
    import pyminit.tools
    if bb is None:
        if ((fun is None) or (grad is None)):
            print("Error: ('fun' and 'grad') or 'bb' must be provided")
            return
        else:
            bb = lambda x, mode=0, context=None: pyminit.tools.fgBB(x, mode=mode, context=context, fun=fun, grad=grad) 
    if L0 is None:
        print("Error: the lower bound  'L0' must be provided")
        return
            
    result = levelBundle(x0, L0, bb, bbContext=bbContext, appSolver=appSolver, appSolverContext=appSolverContext, bundle=bundle, lb=lb, ub=ub, gamma=gamma, xLog=xLog, globalLog=globalLog, tol=tol, maxIter=maxIter) 
    return result
