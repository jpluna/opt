import numpy as np
import pyminit.common as common
import time

def LPsolverGRB(xk, bundle, context, dispose=None):
    import gurobipy as grb

    nb = len(bundle)
    n = len(bundle[0][1][0])

    m = grb.Model('cutting')
    m.setParam('LogtoConsole', 0)
    # Setting variables
    xIndex = tuple(range(n))
    cutIndex = tuple(range(nb))
    x = m.addVars(xIndex, vtype=grb.GRB.CONTINUOUS, lb=context['lb'], ub=context['ub'], name='x')
    t = m.addVar(lb = -grb.GRB.INFINITY,vtype=grb.GRB.CONTINUOUS, name='t')

    # Cutting planes
    for(bId, (xi, fi, si)) in bundle: 
        m.addConstr(fi - np.dot(si, xi) + grb.quicksum(si[j] * x[j] for j in xIndex) <= t, name=str(bId)) 

    # Objective function
    m.setObjective(t, grb.GRB.MINIMIZE)

    # Solving the problem
    m.optimize()
    xk = np.zeros(n)
    for i in xIndex:
        xk[i] = x[i].X
    tk = t.X
    # m.write('jj.lp')
    if m.status == grb.GRB.Status.OPTIMAL:
        status = 0
    else:
        status = 1
    return xk, tk, status
        
def LPsolverGRB2(xk, bundle, context, dispose=False):
    import gurobipy as grb
    # if 'model' in context: print(context['model'])
    if not dispose:
        n = len(bundle[0][1][0])
        xIndex = tuple(range(n))
        if 'model' not in context:
            m = grb.Model('cutting')
            m.setParam('LogtoConsole', 0)
            # Setting variables
            x = m.addVars(xIndex, vtype=grb.GRB.CONTINUOUS, lb=context['lb'], ub=context['ub'], name='x')
            t = m.addVar(lb = -grb.GRB.INFINITY,vtype=grb.GRB.CONTINUOUS, name='t')
            m.update()
            # Objective function
            m.setObjective(t, sense=grb.GRB.MINIMIZE)
            context['model'] = m
            context['x'] = x
            context['t'] = t
            context['bundleIds'] = []
        else:
            m = context['model']
            x = context['x']
            t = context['t']


        # Cutting planes
        for(bId, (xi, fi, si)) in bundle:
            if bId not in context['bundleIds']: 
                m.addConstr(fi - np.dot(si, xi) + grb.quicksum(si[j] * x[j] for j in xIndex) <= t, name=str(bId)) 
                context['bundleIds'].append(bId)


        # Solving the problem
        m.optimize()
        xk = np.zeros(n)
        for i in xIndex:
            xk[i] = x[i].X
        tk = t.X
        # m.write('jj.lp')
        if m.status == grb.GRB.Status.OPTIMAL:
            status = 0
        else:
            status = 1
        # print('status={}'.format(status))
    else:
        # context['model'].terminate()
        # context['model'].reset()
        context['model'].remove(context['x'])
        context['model'].remove(context['t'])
        del context['model']
        
        grb.disposeDefaultEnv()
        print(grb.models())
        print('disposing')
        xk = None
        tk = None
        status = 0
    # print(tk)

    return xk, tk, status

def cuttingPlane(x0, bb, bbContext=None,
        appSolver=None, appSolverContext=None, 
        bundle=[],
        lb=None, ub=None,
        xLog=None, globalLog=None, 
        tol=1e-5, maxIter=50): 
    '''
 
    bb: non smooth black box
    x0: initial point
    xLog: path file for recording the sequence rengerated by the algorithm
    globalLog: path file for general information 
    maxIter:
    '''

    startTime = time.time()
    # Setting default values to None declared arguments
    if xLog is not None: xLogFile = open(xLog, 'w')
    if globalLog is not None: globalLogFile = open(globalLog, 'w')

    if appSolver is None:
        appSolver = LPsolverGRB2
        # appSolver = LPsolverGRB
        if appSolverContext is None:
            appSolverContext = {'lb': lb, 'ub':ub}


    if ('lb' not in appSolverContext)  or (appSolverContext['lb'] is None):
        appSolverContext['lb'] = -10000.0

    if ('ub' not in appSolverContext)  or (appSolverContext['ub'] is None):
        appSolverContext['ub'] = 10000.0


    xk = x0
    fk, sk, status = bb(xk, mode=2, context=bbContext)
    newCutId = 1
    if status == 0:
        bundle.append((newCutId, (xk, fk, sk))) # defining the initial bundle
    else:
        return  common.result_class()
    x_best = xk
    f_best = fk

    iterK = 0
    deltak = tol  + 1 # ensuring the first iteration
    if xLog is not None: 
        xLogFile.write( ','.join(['[k={}] x='.format(iterK)] + [str(val) for val in xk]) + '\n')
    if globalLog is not None: 
        globalLogFile.write('[k={}]f_best={}, f(x_k+1)={}, deltak={}\n'.format(iterK, f_best, fk, deltak))
    while ((iterK < maxIter) and (deltak > tol)): 
        iterK += 1
        # print(bundle)
        xk, tk, appSolverStatus = appSolver(xk, bundle, context=appSolverContext, dispose=False)
        if appSolverStatus != 0:
            print('Error: appSolverStatus ended with status = {}'.format(appSolverStatus))
            status = appSolverStatus
            appSolver(xk, bundle, context=appSolverContext, dispose=True)
            return common.result_class(x_best=x_best, f_best=f_best, x_last=xk, f_last=fk, status=status)
        else:
            fk, sk, bbStatus = bb(xk, mode=2, context=bbContext)
            if bbStatus == 0:
                deltak = fk - tk
                newCutId += 1
                bundle.append((newCutId, (xk, fk, sk))) 
                if fk < f_best:
                    f_best = fk
                    x_best = np.array(xk)
            else:
                print('Error: Black Box ended with status = {}'.format(status))
                status = bbStatus
                return common.result_class(x_best=x_best, f_best=f_best, x_last=xk, f_last=fk, status=status)

        if xLog is not None: 
            xLogFile.write( ','.join(['[k={}] x='.format(iterK)] + [str(val) for val in xk]) + '\n')
        if globalLog is not None: 
            globalLogFile.write('[k={}]f_best={}, f(x_k+1)={}, deltak={}\n'.format(iterK, f_best, fk, deltak))

    if (deltak > tol):
        status = 1
        if globalLog is not None: globalLogFile.write('Maximum number of iterations {} reached without optimality\n'.format(iterK))
    else:
        status = 0

    if xLog is not None: xLogFile.close()
    if globalLog is not None: globalLogFile.close()
    endTime = time.time()
    
    appSolver(xk, bundle, context=appSolverContext, dispose=True)
    return common.result_class(x_best=x_best, f_best=f_best, x_last=xk, f_last=fk, status=status, deltak=deltak, nIter=iterK, time=endTime-startTime)

def minimize(x0, fun=None, grad=None, bb=None, bbContext=None,
        appSolver=None, appSolverContext=None, 
        bundle=[],
        lb=None, ub=None,
        xLog=None, globalLog=None, 
        tol=1e-5, maxIter=100): 
    import pyminit.tools
    if bb is None:
        if ((fun is None) or (grad is None)):
            print("Error: ('fun' and 'grad') or 'bb' must be provided")
            return
        else:
            bb = lambda x, mode=0, context=None: pyminit.tools.fgBB(x, mode=mode, context=context, fun=fun, grad=grad)

    result = cuttingPlane(x0, bb, bbContext=bbContext, appSolver=appSolver, appSolverContext=appSolverContext, bundle=bundle, lb=lb, ub=ub, xLog=xLog, globalLog=globalLog, tol=tol, maxIter=maxIter) 
    return result
