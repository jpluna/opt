import numpy as np

def maxq(x, mode=0, context=None):
    '''
    f(x) = max xi^2
    the subgradient is 2*xi*ei, where i is the first element of argmax{xi^2}
    '''
    status = 0
    x2 = x * x
    argmax = np.argmax(x2)
    if mode == 0:
        return x2[argmax], status
    elif mode == 1:
        g = np.zeros_like(x)
        g[argmax] = 2 * x[argmax]
        return g, status
    elif mode == 2:
        g = np.zeros_like(x)
        g[argmax] = 2 * x[argmax]
        return x2[argmax], g, status
def chainedLQ(x, mode=0, context=None):
    '''
    f(x) = sum _{0,..n-2}max{-xi-x(i+1), -xi-x(i+1) + (xi^2 +x(i+1)^2 -1 )}  
    the subgradient is 2*xi*ei, where i is the first element of argmax{xi^2}
    '''
    status = 0
    xx = -x[:-1] - x[1:] 
    yy = (x[:-1] ** 2) + (x[1:] ** 2) - 1
    yy = np.select([yy>0], [yy])
    
    if mode == 0:
        return sum(xx + yy), status
    elif mode == 1:
        g = -2 * np.ones_like(x)
        g[0] = -1
        g[-1] = -1
        for i in range(len(yy)):
            if yy[i] > 0:
                g[i] += 2 * x[i]
                g[i + 1] += 2 * x[i + 1]
        return g, status
    elif mode == 2: 
        g = -2 * np.ones_like(x)
        g[0] = -1
        g[-1] = -1
        for i in range(len(yy)):
            if yy[i] > 0:
                g[i] += 2 * x[i]
                g[i + 1] += 2 * x[i + 1]
        return sum(xx + yy), g, status
