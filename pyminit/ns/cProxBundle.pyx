#!python
#cython: language_level=3


from libc.stdlib cimport malloc, free 
from libc.string cimport strcpy

cdef extern from "blackBoxTools.h": 
    ctypedef int (*blackBoxP)(const double *x, const int n,  const int mode, double *fval, double *g, void *context)
            
cdef extern from "proxDefaults.h": 
    ctypedef struct defaultStoppingContext: 
        double *targetValue
        double *optimalValue

cdef extern from "proxBundle.h": 
    ctypedef struct proxBundle: 
        int maxSize 
        int size 
        int absLastId #//absoulte value of newest (last) cut id
        int **cut #// cut[i] = [idcut, position].  
        double **c1 #// c1[position] 
        double  *c0 #// c0[position] 
        double  *alpha #// alpha[position] mulitplier associated to the cut 

    ctypedef int bundleManager(proxBundle *bundle, void* context) 

    # ctypedef int (*bundleManagerP)(proxBundle *bundle, void* context) 

    ctypedef int proxLMSolver(double *xk, const int n, double *fk, proxBundle *bundle, double *lb, double *ub, int num_lcon,int *conSize, int **colIndex, double **lhs, int *conSense, double * rhs, double *muk, bundleManager *   bundleManagerFun, void * bundleManagerContext, double *sa,  double *zkprox, double *proxOptVal, double *deltak, double *c0a, void *context) 

    ctypedef struct algorithmState:
        int n
        int *nIter
        int seriousStepPerformed
        double *x_serious
        double *f_serious
        double *x_last
        double *f_last
        double *delta

    ctypedef int stopCriterion(double *sa, int n,  double muk, double tol, algorithmState *state, void *stopCriterionContext) 

    cdef int maxSizeBundleManager(proxBundle *bundle, void* context) 

    cdef int nonZeroMultiplierBundleManager(proxBundle *bundle, void* context)



    cdef int proxBundleMethodMin(double *x0, int n,  blackBoxP bb, void *bbContext, proxLMSolver *solveProxM, void *solveProxMConxtext, bundleManager *  bundleManagerFun, void * bundleManagerContext, proxBundle *bundle, stopCriterion *stopCriterionFun, void *stopCriterionContext, double m, double *lb, double *ub, int num_lcon,int *conSize, int **colIndex, double **lhs, int *conSense, double * rhs, void *context, char *xLog, char *globalLog, double tol, int maxIter, double *x_best, double *f_best,double * x_last, double *f_last, double *deltak, int * nIter, double *time)

cdef extern from "proxSolver.h": 
    cdef int solveProxLMPrimal(double *xk, const int n, double *fk, proxBundle *bundle, double *lb, double *ub, int num_lcon,int *conSize, int **colIndex, double **lhs, int *conSense, double * rhs, double *muk, bundleManager *  bundleManagerFun, void * bundleManagerContext, double *sa,  double *zkprox, double *proxOptVal, double *deltak, double *c0a, void *context)

    cdef int solveProxLMDual(double *xk, const int n, double *fk, proxBundle *bundle, double *lb, double *ub, int num_lcon,int *conSize, int **colIndex, double **lhs, int *conSense, double * rhs, double *muk, bundleManager *  bundleManagerFun, void * bundleManagerContext, double *sa,  double *zkprox, double *proxOptVal, double *deltak, double *c0a, void *context)

    cdef void * initProxMDual2Context()
    cdef int solveProxLMDual2(double *xk, const int n, double *fk, proxBundle *bundle, double *lb, double *ub, int num_lcon,int *conSize, int **colIndex, double **lhs, int *conSense, double * rhs, double *muk, bundleManager *  bundleManagerFun, void * bundleManagerContext, double *sa,  double *zkprox, double *proxOptVal, double *deltak, double *c0a, void *context)
    

## global elements
cdef object py_bb= None #This for the bb function

import numpy as np
cimport numpy as cnp
# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
cnp.import_array()
# bundle
cdef class bundleClass():
    cdef public int maxSize
    cdef public cnp.ndarray cut
    cdef int[:,:] vCut

    cdef public cnp.ndarray c1
    cdef double[:,:] vC1

    cdef public cnp.ndarray c0
    cdef double[:] vC0

    cdef public cnp.ndarray alpha
    cdef double[:] vAlpha
    cdef proxBundle *bundle

    def __init__(self, n, maxSize=10, size=0):
        self.bundle = <proxBundle *>malloc(sizeof(proxBundle))

        self.maxSize = maxSize
        self.bundle.maxSize = maxSize
        self.bundle.size = size
        self.bundle.absLastId = 0

        self.cut = np.zeros((maxSize, 2), dtype=np.intc)
        self.vCut = self.cut

        self.c1 = np.zeros((maxSize, n), dtype=np.double)
        self.vC1 = self.c1

        self.bundle.cut = <int **>malloc(maxSize * sizeof(int *))
        self.bundle.c1 = <double **>malloc(maxSize * sizeof(double *))
        for i in range(maxSize):
            self.bundle.cut[i] = &self.vCut[i,0]
            self.bundle.c1[i] = &self.vC1[i,0]

        self.c0 = np.zeros(maxSize, dtype=np.double)
        self.vC0 = self.c0
        self.bundle.c0 = &self.vC0[0]

        self.alpha = np.zeros(maxSize, dtype=np.double)
        self.vAlpha = self.alpha
        self.bundle.alpha = &self.vAlpha[0]

        self.cut[:,1] = np.arange(maxSize)
        # int maxSize 
        # int size 
        # int **cut #// cut[i] = [idcut, position].  
        # double **c1 #// c1[position] 
        # double  *c0 #// c0[position] 
        # double  *alpha #// alpha[position] mulitplier associated to the cut 
    def __dealloc__(self):
        free(self.bundle.cut)
        free(self.bundle.c1)
        free(self.bundle)
        


# bb
cdef int myBB(double *x, int n, int mode, double *fval, double *g, void *context): 
    cdef int i
    cdef int status
    xx = np.zeros(n)
    for i in range(n):
        xx[i] = x[i]
    # print('inside BB, after =', )
    # for i in range(n):
        # print(', {}'.format(x[i]))
    # print('\n')
    # print('inside BB =', xx)
    if  py_bb != None:
        if mode == 0:
            bbVal, status = py_bb(xx, mode=mode)
            fval[0] = bbVal
        elif mode ==1:
            bbSg, status = py_bb(xx, mode=mode)
            for i in range(n):
                g[i] = bbSg[i]
        else:
            bbVal, bbSg, status = py_bb(xx, mode=mode)
            fval[0] = bbVal
            for i in range(n):
                g[i] = bbSg[i]

        # print('inside BB, after =', xx)
    else:
        print('Null pointer in bb function!!')
        status = -1
    # print('inside BB, after =', )
    # for i in range(n):
        # print(', {}'.format(x[i]))
    # print('\n')
    return status

def minimize(x0, fun=None, grad=None, bb=None, bbContext=None, 
        solveProxM=None, solveProxMConxtext=None,
        proxParam=None, proxParamContext=None,
        bundleManagerFun=None, bundleManagerContext=None,
        bundleClass bundle=None,## It is a bundleClass
        stoppingCriterionFun=None,
        stoppingCriterionContex=None,
        bundleMaxSize=10,
        m=0.5,
        lb=None,
        ub=None,
        LHS=None,
        sense=None,
        RHS=None,
        context=None, 
        xLog=None, globalLog=None, 
        tol=1e-5, maxIter=50,
        targetValue=None,
        optimalValue=None): 

    import pyminit.tools
    import pyminit.common as common
    cdef blackBoxP cBB = NULL
    cdef void *cBBContext=NULL

    cdef void *cContext=NULL
    cdef double ctol=tol
    cdef int status
    
    cdef double *cLb=NULL
    cdef double *cUb=NULL
    cdef double[:] vLb
    cdef double[:] vUb
    cdef proxBundle *cBundle= NULL

    cdef bytes py_bytes1, py_bytes2

    ## initial point

    x0 = np.array(x0)
    cdef int n = len(x0)
    cdef double[:] vX0 = x0

    ## bounds
    if lb is not None:
        pyLb = np.ones(n)
        if isinstance(lb, (list, np.ndarray)): 
            pyLb[:] = np.array(lb)
        else:
            pyLb = lb * pyLb
        vLb = pyLb
        cLb = &vLb[0]
            
    if ub is not None:
        pyUb = np.ones(n)
        if isinstance(ub, (list, np.ndarray)): 
            pyUb[:] = np.array(ub)
        else:
            pyUb = ub * pyUb
        vUb = pyUb
        cUb = &vUb[0]

    cdef int num_lcon=0
    cdef int *conSize=NULL
    cdef int [:] vconSize
    cdef int **colIndex=NULL
    cdef int [:] vcolIndex
    cdef double **lhs=NULL
    cdef double[:] vlhs

    cdef int *conSense=NULL
    cdef int [:] vconSense

    cdef double *rhs=NULL
    cdef double[:] vrhs

    if ((LHS is not None) and (sense is not None) and (RHS is not None)):
        num_lcon = len(sense)

        nnLHS = LHS!=0

        pyConSize = np.array(nnLHS.sum(axis=1), dtype=np.int32)
        # print('pyconsize = {}'.format( pyConSize))
        vconSize = pyConSize
        conSize = &vconSize[0]

        nnz_LHS = LHS[nnLHS]
        nnz_iLHS = np.array(num_lcon *[np.arange(n)], dtype=np.int32)[nnLHS]
        vcolIndex  = nnz_iLHS
        vlhs = nnz_LHS
        # print('juanpa',nnz_LHS)
        colIndex = <int **>malloc(num_lcon*sizeof(int *))
        lhs = <double **>malloc(num_lcon*sizeof(double *))

        colIndex[0] = &vcolIndex[0]
        lhs[0] = &vlhs[0]
        for i, ii in enumerate(pyConSize.cumsum()[:-1], start=1):
            colIndex[i] = &vcolIndex[ii]
            lhs[i] = &vlhs[ii]

        vconSense = np.array(sense, dtype=np.int32)
        conSense = &vconSense[0]

        vrhs = RHS
        rhs = &vrhs[0]



    ## bb
    if bb is None:
        if ((fun is None) or (grad is None)):
            print("Error: ('fun' and 'grad') or 'bb' must be provided")
            return common.result_class(status=1)
        else:
            bb = lambda x, mode=0, context=None: pyminit.tools.fgBB(x, mode=mode, context=context, fun=fun, grad=grad)

    global py_bb
    py_bb = lambda x, mode=0: bb(x, mode=mode, context=bbContext)
    cBB = &myBB

    ## prox solver
        # solveProxM=None, solveProxMConxtext=None,
    cdef void *cSolveProxMConxtext
    if solveProxMConxtext is None:
        cSolveProxMConxtext = NULL

    cdef proxLMSolver *cSolveProxM
    if solveProxM is not None:
        if isinstance(solveProxM, str):
            if solveProxM == 'primal':
                cSolveProxM = &solveProxLMPrimal
                print('primal solver chosen')
            elif solveProxM == 'dual': 
                print('dual solver chosen')
                if ((lb is not None) or (ub is not None)):
                    print('warning: bounds (lb and/or ub) are set. primal solver enabled')
                    cSolveProxM = &solveProxLMPrimal
                else: 
                    cSolveProxM = &solveProxLMDual
            elif solveProxM == 'dual2':
                print('dual2 solver chosen')
                if ((lb is not None) or (ub is not None)):
                    print('warning: bounds (lb and/or ub) are set. primal solver enabled')
                    cSolveProxM = &solveProxLMPrimal
                else: 
                    cSolveProxM = &solveProxLMDual2
                    if cSolveProxMConxtext == NULL:
                        print('warning: non NULL context required.  A default context will be used. ')
                        cSolveProxMConxtext = initProxMDual2Context()
                        
            else: 
                cSolveProxM = NULL
        else: 
            cSolveProxM = NULL
    else: 
        cSolveProxM = NULL 

    ## bunger manager
    cdef bundleManager *  cBundleManagerFun

    cdef void * cBundleManagerContext
    
    if bundleManagerFun is not None:
        if isinstance(bundleManagerFun, str):
            if bundleManagerFun == 'maxBundle':
                cBundleManagerFun = &maxSizeBundleManager
            elif bundleManagerFun == 'lagrangianWeight':
                cBundleManagerFun = &nonZeroMultiplierBundleManager
            else:
                cBundleManagerFun = NULL
        else:
            cBundleManagerFun = NULL
    else:
        cBundleManagerFun = NULL

    if bundleManagerContext is None:
        cBundleManagerContext = NULL


    ## defining bundle
    # if bundle is None:
        # bundle = {'maxSize': bundleMaxSize, 
                # 'size': 0,
                # 'cut': np.zeros((bundleMaxSize,2),dtype=np.intc),
                # 'c1': np.zeros((bundleMaxSize, n), dtype=np.double),
                # 'c0': np.zeros(bundleMaxSize)}
    # bundle['cut'][:,1] = np.arange(bundleMaxSize)
    # cBundle = <proxBundle *>malloc(sizeof(proxBundle))
    # cBundle.maxSize = bundle['maxSize']
    # cBundle.size = bundle['size']
    # cBundle.alpha = NULL
    # cdef double[:] vErr= bundle['c0']
    # cBundle.c0 = &vErr[0]

    # cBundle.cut = <int **>malloc(cBundle.maxSize * sizeof( int *))
    # cutAux = bundle['cut'].reshape(2 * cBundle.maxSize)
    # cdef int[:] vCut= cutAux

    # cBundle.c1 = <double **>malloc(cBundle.maxSize * sizeof( double *))
    # c1Aux = bundle['c1'].reshape(n * cBundle.maxSize)
    # cdef double[:] vSgCut= c1Aux

    # for i in range(cBundle.maxSize):
        # cBundle.cut[i] = &vCut[2 * i]
        # cBundle.c1[i] = &vSgCut[n * i]
    if bundle is None:
        myBundle =  bundleClass(len(x0), maxSize=bundleMaxSize)
        cBundle = myBundle.bundle
    else:
        cBundle = bundle.bundle

    ## stopping Criterion
    cdef stopCriterion *stopCriterionFun
    cdef void *stopCriterionContext
    cdef defaultStoppingContext *cStopContext
    cStopContext = <defaultStoppingContext *>malloc(sizeof(defaultStoppingContext))
    cStopContext.targetValue = NULL
    cStopContext.optimalValue = NULL

    if stoppingCriterionFun is None: 
        stopCriterionFun = NULL


    if stoppingCriterionContex is not None:## JP: freeing memory is not yed coded
        stopCriterionContext = NULL ## JP: needs more work
    elif ((targetValue is not None) or (optimalValue is not None)):
        if (targetValue is not None):
            cStopContext.targetValue = <double *>malloc(sizeof(double))
            cStopContext.targetValue[0] = targetValue
        if (optimalValue is not None):
            cStopContext.optimalValue = <double *>malloc(sizeof(double))
            cStopContext.optimalValue[0] = optimalValue
        stopCriterionContext = <void *> cStopContext
    else:
        stopCriterionContext = NULL




    ## log
    cdef char *cxLog = NULL 
    if xLog is not None: 
        py_bytes1 = xLog.encode()
        cxLog  = py_bytes1

    cdef char *cglobalLog = NULL
    if globalLog is not None: 
        py_bytes2 = globalLog.encode()
        cglobalLog  = py_bytes2

    ## Output
    cdef int nIter = 0
    x_best = np.zeros(n)
    cdef double [:]vx_best = x_best
    x_last = np.zeros(n)
    cdef double [:]vx_last = x_last
    cdef double f_best = 0.0
    cdef double f_last = 0.0
    cdef double time = 0.0
    cdef double deltak = 0.0


    status = proxBundleMethodMin(&vX0[0], n, cBB, cBBContext, cSolveProxM, cSolveProxMConxtext, cBundleManagerFun,  cBundleManagerContext, cBundle, stopCriterionFun, stopCriterionContext, m, cLb, cUb, num_lcon,conSize, colIndex,  lhs,conSense, rhs, cContext, cxLog, cglobalLog, ctol, maxIter, &vx_best[0], &f_best, &vx_last[0], &f_last, &deltak, &nIter, &time)
    return x_best, f_best, x_last, f_last, deltak, nIter, time, status

