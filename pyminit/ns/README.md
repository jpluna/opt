# NS
The nonsmooth optimization module. 
Currently we have the following algorithms for nonsmooth optimziation
- [Cutting Plane Method](./cuttingPlane.md)
- [Proximal Bundle Method](./proxBundle.md)
- [Level Bundle Method](./levelREADME.md)

All these methods return a `result` object that has some result information
- `x_best`: Best point found by the algorithm.
- `f_best`: Value at `x_best'
- `x_last`: Last point found by the algorithm.
- `f_best`: Value at `x_last'
- `deltak`: Gap value.
- `status`: 0 if algorithm ended with optimality.
- `nIter`: Number of iterations performed
- `time`: time in seconds used by the algorithm
