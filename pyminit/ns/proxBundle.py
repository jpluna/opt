import numpy as np
import pyminit.common as common



def minimize(x0, fun=None, grad=None, bb=None, bbContext=None, fg=None, fgContext=None,
        solveProxM=None, solveProxMConxtext=None,
        proxParam=None, proxParamContext=None,
        bundleManagerFun=None, bundleManagerContext=None,
        bundle=None,
        bundleMaxSize=5,
        mu0=1,
        m=0.5,
        lb=None,
        ub=None,
        LHS=None,
        sense=None,
        RHS=None,
        context=None, 
        xLog=None, globalLog=None, 
        stopCriterion=None,
        outputMessageLevel=4,#bmrio
        bmrioAlg=1,#bmrio
        inexactError=0,#bmrio
        targetValue=None,
        optimalValue=None,
        tol=1e-5, maxIter=10, method='python'):
    import pyminit.tools
    import pyminit.ns.pyProxBundle as pypb
    from pyminit.ns import cProxBundle as cpb
    from pyminit.ns import cbmrio as bmrio

    if ((fg is not None) and (bb is None)): ## JP: temporal hack
        bb = lambda x, mode=0, context=None: pyminit.tools.fgBB(x, mode=mode, context=context, fg=fg)


    
    if method == 'C': 
        x_best, f_best, x_last, f_last, deltak, nIter, time, status = cpb.minimize(x0, 
                fun=fun,
                grad=grad,
                bb=bb,
                bbContext=bbContext,
                solveProxM=solveProxM,
                solveProxMConxtext=solveProxMConxtext,
                proxParam=proxParam,
                proxParamContext=proxParamContext,
                bundleManagerFun=bundleManagerFun,
                bundleManagerContext=bundleManagerContext,
                bundle=bundle,
                bundleMaxSize=bundleMaxSize,#py
                m=m,
                lb=lb,#py
                ub=ub,#py
                LHS=LHS,
                sense=sense,
                RHS=RHS,
                context=context,
                xLog=xLog,
                globalLog=globalLog,
                tol=tol,
                maxIter=maxIter, 
                targetValue=targetValue, 
                optimalValue=optimalValue
                ) 
        result = common.result_class(x_best=x_best, f_best=f_best, x_last=x_last, f_last=f_last, deltak=deltak, nIter=nIter, time=time, status=status)

    elif method == 'bmrio': 
        x_best, f_best, x_last, f_last, deltak, nIter, time, status = bmrio.minimize(x0, 
                fun=fun,
                grad=grad,
                bb=bb,
                bbContext=bbContext,
                bundleMaxSize=bundleMaxSize,#py
                lb=lb,#py
                ub=ub,#py 
                outputMessageLevel=outputMessageLevel,
                bmrioAlg=bmrioAlg,
                inexactError=inexactError,
                globalLog=globalLog,
                tol=tol,
                maxIter=maxIter
                ) 
        result = common.result_class(x_best=x_best, f_best=f_best, x_last=x_last, f_last=f_last, deltak=deltak, nIter=nIter, time=time, status=status)

    else:
        result = pypb.minimize(x0, 
                fun=fun,
                grad=grad,
                bb=bb,
                bbContext=bbContext,
                solveProxM=solveProxM,
                solveProxMConxtext=solveProxMConxtext,
                proxParam=proxParam,
                proxParamContext=proxParamContext,
                bundleManagerFun=bundleManagerFun,
                bundleManagerContext=bundleManagerContext,
                bundle=bundle,
                mu0=mu0,
                m=m,
                context=context,
                xLog=xLog,
                globalLog=globalLog,
                tol=tol,
                maxIter=maxIter
                )

    return result
