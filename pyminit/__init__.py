'''
PyMinIt
=======

This is a python library providing tools por minimizing functions. The package provides the following submodules:
    - tools
    - unc 
        Unconstrained Optimization.
    - ns 
        Nonsmooth Optimization.
'''


# __all__=['unc', 'ns']

# from . import *
