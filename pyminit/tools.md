# tools
This module provides some helpful routines for optimization.

- checking gradients
- [Checking Subgradients](#checking-subgradient)
- [Plotting Level Curves](#plotting-level-curves)
- plotting 3D function


## Checking Subgradient
```python
'''
Checks if the  result of the subgradient function 'subgrad(x) --> s'  corresponds to a 
subgradient of 'fun(x) --> fval'.  If 'bb' is provided, 'fun' and 'subgrad' are discarded.

def checkNSBB(x, fun=None, subgrad=None, bb=None, bbContext=None, nSample=5, tol=1e-5):
	return: True o False, according if the subgradient is computed correctly
	or not.
'''
```

### Example
```python
import numpy as np
import pyminit.tools

def f(x):
	return np.abs(x).sum() - 1

def s(x):
	sg = np.ones_like(x)
	sg[x<0] = -1.0
	return sg

def wrongS(x):
	sg = np.ones_like(x)
	return sg
```

```python
x0 = np.random.randn(5)

pyminit.tools.checkNSBB(x0, fun=f, subgrad=s)
Out[1]: True

pyminit.tools.checkNSBB(x0, fun=f, subgrad=wrongS)
Out[2]: False
```

A `False` result ensures the sugradient is wrong, however a `True` result may
represent a 'false positive'. In order to reduce probability of 'false positive',
we can use the argument `nSample` (defalt value 5)

```python
x0 = np.random.randn(5)

pyminit.tools.checkNSBB(x0, fun=f, subgrad=wrongS, nSample=100)
Out[2]: False
```
## Plotting Level Curves

```python
    '''
def plot(fun=None, bb=None, x=None, y=None, path=None, files=None, save=None, alpha=1, linewidth=1, gridSize=100, curves=False, levels=50):
    Plots the level curves of the function described by fun or bb. At least one of them should be provided. In case both are provided 'bb' has precedence.
        fun(x): function that returns a number
        bb(x,mode=0, context=None): returns for mode==0 the pair (value_function, status)
    In the plot is also included a list of sequences described by path and files. Thus, at least some of the arguments x, y, path or files:
        x: [x_min, x_max]
        y: [y_min, y_may]
        path: is a list of lists. Each element  is of  the form [xx, yy], where xx is a list
        the firts entries oa list of points and yy is the a list of thre corresponding second
        coordinates
        files: list of log files that are produced by the method 'descent 
    save: string for a figure name.
    '''
```
### Example
```python
import numpy as np
import pyminit.tools

def f(x):
	return np.abs(x).sum() - 1
pyminit.tools.plot(fun=f, x=[-1,1])
```
![](./plt_ex1.png)


