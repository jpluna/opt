###
import pynstestf as pf
import pyminit.ns.cuttingPlane as cp
import numpy as np
###
codes = pf.codes

###
for cc in [codes[5]]:
# for cc in codes[:2]:
# for cc in ['MAXQUAD']:
    prob = pf.problem(cc)
    print(prob.n)
    res = cp.minimize(prob.x0, bb=prob.bb, lb=-10.0, ub=10.0, xLog='./x.log', globalLog='./g.log')
    print('{}: x0:{}\n res --> {}'.format(cc, prob.x0, res))

###
