import numpy as np
import pyminit.ns.testF as nsTestF
import pyminit.ns.proxBundle as pb


bb = nsTestF.chainedLQ
bb = nsTestF.maxq
x0 = 2 * np.ones(2)


# print(pb.minimize(x0, bb=bb, method='C'))
# print(pb.minimize(x0, bb=bb, solveProxM='primal', method='C', tol=1e-5, maxIter=50))
print(pb.minimize(x0, bb=bb, solveProxM='dual', method='C', xLog='x.log', globalLog='g.log', targetValue=None, maxIter=15))
# print(pb.minimize(x0, bb=bb, method='C', xLog='x.log', globalLog='g.log', targetValue=None))
# print(pb.minimize(x0, bb=bb, method='bmrio', xLog='x.log', globalLog='g.log', targetValue=None))

