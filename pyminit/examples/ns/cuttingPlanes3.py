import numpy as np
import pyminit.ns.testF as nsTestF
import pyminit.ns.cuttingPlane as cp


bb = nsTestF.chainedLQ
bb = nsTestF.maxq
x0 = 2 * np.ones(2)

x , fval, status = cp.cuttingPlane(x0, bb, xLog='./x.log', globalLog='g.log', tol=1e-5, maxIter=50) 
print(x , fval, status)


import pyminit.tools
import matplotlib.pyplot as plt

pyminit.tools.plot(bb=bb, files=[('./x.log', 'r')])
plt.show()
