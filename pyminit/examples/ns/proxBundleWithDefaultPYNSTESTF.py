###
import pynstestf as pf
import pyminit.ns.proxBundle as pb
import numpy as np
###
codes = pf.codes

# for cc in codes[2:3]:
for cc in [codes[3]]:
# for cc in ['MAXQ']:
    prob = pf.problem(cc)
    res = pb.minimize(prob.x0, bb=prob.bb, xLog='./x.log', globalLog='./g.log',maxIter=50)
    print('{}: x0:{}\n res --> {}'.format(cc, prob.x0, res))

