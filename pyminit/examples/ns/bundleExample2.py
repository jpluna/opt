import numpy as np
import pyminit.ns.testF as nsTestF
import pyminit.ns.proxBundle as pb


bb = nsTestF.maxq
bb = nsTestF.chainedLQ
x0 = 2 * np.ones(2)


print(pb.minimize(x0, bb=bb, m=0.5, xLog='./bx.log', globalLog='bg.log', tol=1e-8, maxIter=50, method='C'))

