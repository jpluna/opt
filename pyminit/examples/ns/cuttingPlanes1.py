import numpy as np
import pyminit.ns.testF as nsTestF
import pyminit.ns.cuttingPlane as cp


bb = nsTestF.maxq
bb = nsTestF.chainedLQ
x0 = 2 * np.ones(2)

x , fval, status = cp.cuttingPlane(x0, bb)
print(x , fval, status)

