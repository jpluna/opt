import numpy as np
import pyminit.ns.testF as nsTestF
import pyminit.ns.proxBundle as pb


bb = nsTestF.maxq
bb = nsTestF.chainedLQ
x0 = 2 * np.ones(2)


# print(pb.minimize(x0, bb=bb, m=0.5, xLog='./bx.log', globalLog='bg.log', tol=1e-8, maxIter=50, method='C'))
# print(pb.minimize(x0, bb=bb, m=0.5, xLog='./bx.log', globalLog='bg.log', tol=1e-8, maxIter=50, lb=-5, method='C'))
print(pb.minimize(x0, bb=bb, m=0.5, xLog='./bx.log', globalLog='bg.log', tol=1e-8, maxIter=50, 
    lb=-5, LHS=np.ones((1,2)), sense=np.array([1]), RHS=np.ones(1), method='C'))

import pyminit.tools
import matplotlib.pyplot as plt
pyminit.tools.plot(bb=bb, files=[('./bx.log', 'r')])
plt.show()
