###
import pynstestf as pf
import pyminit.ns.levelBundle as nslb
import numpy as np
###
codes = pf.codes

###
# for cc in [codes[4]]:
# for cc in codes[:3]:
# for cc in codes[2:3]:
for cc in ['MAXQ']:
    prob = pf.problem(cc)
    L0=-100.0
    res = nslb.minimize(prob.x0, bb=prob.bb, L0=L0, lb=-100, ub=100, xLog='./x.log', globalLog='./g.log', maxIter=200)
    print('{}: x0:{}\n res --> {}'.format(cc, prob.x0, res))
###
