
import numpy as np
import pyminit.unc.descent

def fun(x):
	val = (x[0] - 1) ** 2 + (np.dot(x,x) - 0.25) ** 2
	return val
x0 = np.ones(2)
# x, f, g, status = pyminit.unc.descent.min(x0, fun=fun, globalLog='./jj.log', xLog='x.log')
x, f, g, status = pyminit.unc.descent.min(x0, fun=fun, dd='lm-bfgs', ls='wolfe', globalLog='./jj.log', xLog='x.log')

import pyminit.tools
pyminit.tools.plot(fun=fun, files=[('x.log', 'r')], save='jj.png')
