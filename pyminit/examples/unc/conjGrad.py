import numpy as np
import pyminit.unc.descent
import pyminit.unc.testF

bb = pyminit.unc.testF.quad
bbContext = np.array([[1,2],[2,5]])
x0 = np.ones(2)
x, f, g, status = pyminit.unc.descent.min(x0, bb=bb, bbContext=bbContext, dd='conjugatedGradient',  globalLog='./jj.log', xLog='x.log')
# x, f, g, status = pyminit.unc.descent.min(x0, bb=bb, bbContext=bbContext, dd='lm-bfgs', ddContext={'size':2},  globalLog='./jj.log', xLog='x.log')

import pyminit.tools
pyminit.tools.plot(bb=bb, files=[('x.log', 'r')], save='jj.png')
