import numpy as np

def fgBB(x, mode=0, context=None, fun=None, grad=None, fg=None):
    status = 0
    if fg is not None:
        fval, g = fg(x)
        if mode == 0:
            return fval, status
        elif mode == 1:
            return g, status
        elif mode == 2:
            return fval, g, status
    else:
        if mode == 0:
            fval = fun(x)
            return fval, status
        elif mode == 1:
            g = grad(x)
            return g, status
        elif mode == 2:
            fval = fun(x)
            g = grad(x)
            return fval, g, status
            

def numBB(x, mode=0, context=None, fun=None, delta=0.0001):
    val = fun(x)
    if mode == 0:
        return val, 0
    else:
        grad = np.zeros_like(x)
        xx = x[:]
        xx[0] += delta
        ff = fun(xx)
        grad[0] = (ff - val) / delta
        
        for i in range(1, len(x)):
            xx[i - 1] -= delta
            xx[i] += delta
            ff = fun(xx)
            grad[i] = (ff - val) / delta
        if mode == 1:
            return grad, 0
        elif mode == 2:
            return val, grad, 0

def checkBB(bb, x, h=1e-5, tol=1e-3):
    '''
    checkBB(bb, x, h=1e-5, tol=1e-3):
        tol: 
    return: True/False
    '''
    import numpy as np
    n = len(x)
    fx, g, status = bb(x, mode=2)
    gnum = np.zeros(n)
    aux = np.eye(n)
    for i in range(n):
        fxx, status = bb(x + h * aux[i], mode = 0)
        gnum[i] = (fxx - fx) / h
    vErrror = np.abs(g - gnum)
    error = max(vErrror)
    if error > tol: 
        print('Grandient entries with possible errors:', np.arange(n)[vErrror>tol])
    return (error <= tol)

        


def checkNSBB(x, fun=None, subgrad=None, bb=None, bbContext=None, nSample=5, tol=1e-5):
    '''
    Checks if the  result of the subgradient function 'subgrad(x) --> s, status'  corresponds to a 
    subgradient of 'fun(x) --> fval, status'.  If 'bb' is provided, 'fun' and 'subgrad' are discarded.
    
    def checkNSBB(x, fun=None, subgrad=None, bb=None, bbContext=None, nSample=5, tol=1e-5):
        return: True o False, according if the subgradient is computed correctly
        or not.
    '''
    import numpy as np
    if bb is None:
        if ((fun is not None) and (subgrad is not None)):
            bb = lambda x, mode, context=None: fgBB(x, mode=mode, context=context, fun=fun, grad=subgrad)
        else:
            print('Error: (fun and subgrad) or bb should be provided')

    fx, g, status = bb(x, mode=2, context=bbContext)
    diff = np.zeros(nSample)
    if status != 0:
        print('black box error: ' + int(status) + '\n')
        return False
    else: 
        if len(g) != len(x):
            print('black box error: length of  the returned subgradient is {}. It should be {} \n'.format(len(g), len(x)))
            return False
        for i in range(nSample):
            d = 2 * np.random.randn(len(x))  - 1
            y =  x + d
            fy, status = bb(y, mode=0, context=bbContext)
            if status == 0:
                diff[i] = fy - fx - np.dot(g, d)
        return all(diff > -tol)

def plot(fun=None, bb=None, fg=None, x=None, y=None, path=None, files=None, save=None, alpha=1, linewidth=1, gridSize=100, curves=False, levels=50):
    '''
def plot(fun=None, bb=None, x=None, y=None, path=None, files=None, save=None, alpha=1, linewidth=1, gridSize=100, curves=False, levels=50):
    Plots the level curves of the function described by fun or bb. At least one of them should be provided. In case both are provided 'bb' has precedence.
        fun(x): function that returns a number
        bb(x,mode=0, context=None): returns for mode==0 the pair (value_function, status)
    In the plot is also included a list of sequences described by path and files. Thus, at least some of the arguments x, y, path or files:
        x: [x_min, x_max]
        y: [y_min, y_may]
        path: is a list of lists. Each element  is of  the form [xx, yy], where xx is a list
        the firts entries oa list of points and yy is the a list of thre corresponding second
        coordinates
        files: list of log files that are produced by the method 'descent 
    save: string for a figure name.
    '''
    import numpy as np
    import pandas as pd
    import matplotlib.pyplot as plt
    
    if fun is None:
        if ((bb is None) and (fg is None)):
            print('Error: fun or bb must be provided')
        else:
            if bb is not None:
                fun = lambda x: bb(x, mode=0)[0]
            else:
                fun = lambda x: fg(x)[0]

    if files is not None:
        if path is None: path=[]

        for ff, cc in files:
            tab = pd.read_csv(ff, index_col=0, header=None)
            # tab = tab[tab.index.map(lambda x: 'x' in x).values]
            aux = [tab[1].values, tab[2].values]
            path.append((aux, cc))


    xexlist = [] # minimum and maximum of x entties of each path
    yexlist = [] # minimum and maximum of y entties of each path
    fig = plt.figure()
    ax = fig.gca()
    ax.cla()
    if path is not None: 
        for aux, cc in path: 
            xexlist.append(min(aux[0]))
            xexlist.append(max(aux[0]))
            yexlist.append(min(aux[1]))
            yexlist.append(max(aux[1]))
            ax.plot(aux[0], aux[1], 'o--', color=cc, linewidth=linewidth)
            ax.plot(aux[0][0], aux[1][0], 'go')
            ax.plot(aux[0][-1], aux[1][-1], 'ko')

    if ((x is None) and (y is not None)):
        x = y

    if ((y is None) and (x is not None)):
        y = x

    if x is not None: 
        xmin = min(x) 
        xmax = max(x)
        # xexlist.append(min(x)) 
        # xexlist.append(max(x))
    elif path is not None:
        xmin = min(xexlist)
        xmax = max(xexlist)
        lx = xmax - xmin
        xmin = xmin - 0.15 * lx
        xmax = xmax + 0.15 * lx
    else:
        xmin=0
        xmax=1
    ax.set_xlim(xmin, xmax)

    if y is not None:
        ymin = min(y) 
        ymax = max(y)
        # yexlist.append(min(y)) 
        # yexlist.append(max(y))
    elif path is not None:
        ymin = min(yexlist)
        ymax = max(yexlist)
        ly = ymax - ymin
        ymin = ymin - 0.15 * ly
        ymax = ymax + 0.15 * ly
    else:
        ymin=0
        ymax=1
    ax.set_ylim(ymin, ymax)

    # if len(xexlist)>0  or len(yexlist)>0:
        # if len(xexlist)>0:
            # xmin = min(xexlist)
            # xmax = max(xexlist)
            # if len(yexlist)>0:
                # ymin = min(yexlist)
                # ymax = max(yexlist)
            # else:
                # ymin= xmin
                # ymax = xmax
        # else:
            # ymin = min(yexlist)
            # ymax = max(yexlist)
            # xmin= ymin
            # xmax = ymax
    # else:
        # ymin = 0
        # xmin = 0
        # ymax = 1
        # xmax = 1

    # lx = xmax - xmin
    # ly = ymax - ymin
    # xa = [xmin - (0.15 * lx), xmax + (0.15 * lx)]
    # ya = [ymin - (0.15 * ly), ymax + (0.15 * ly)]
    # xx = np.arange(xa[0], xa[1], lx/50)
    # yy = np.arange(ya[0], ya[1], ly/50)
    xx = np.linspace(xmin, xmax, gridSize)
    yy = np.linspace(ymin, ymax, gridSize)
    XX, YY = np.meshgrid(xx, yy)
    # Note the peculiar estructure of XX and YY
    ## XX has all rows equal (and so constrant columns)
    ## YY has  constant rows (and so all columns are the same)
    ZZ = list()
    for xy in zip(XX, YY):
        ZZ += [[fun(np.array([xxx, xy[1][0]])) for xxx in xy[0]]]

    if curves:
        ax.contour(XX, YY, np.array(ZZ), levels, alpha=alpha)
    else: 
        ax.contourf(XX, YY, np.array(ZZ), levels, alpha=alpha)
    # plt.axis('equal')
    if save is not None:
        plt.savefig(save)

    return fig

def plot3D(fun=None, bb=None, x=None, y=None, save=None): 
    '''
    plot3D(bb, x, y, save=None)
        fun: funtion for plotting
        bb: blackBox associated to fun
        x=[x_min, x_max], if None, x=[0,1]
        y=[y_min, y_max], if None, y=[0,1]
        save: str
            file name for saving figure
    return: fig
    '''
    if fun is None:
        if bb is None:
            print('Error: fun or bb must be provided')
        else:
            fun = lambda x: bb(x, mode=0)[0]
    from mpl_toolkits.mplot3d import Axes3D
    import matplotlib.pyplot as plt
    if x is None: x = [0,1]
    if y is None: y = [0,1]
    xx = np.linspace(x[0], x[-1], 50)
    yy = np.linspace(y[0], y[-1], 50)
    nx = len(xx)
    ny = len(yy)
    [X, Y] = np.meshgrid(xx, yy)
    Z = np.zeros((nx, ny))
    for i in range(nx):
        for j in range(ny):
            Z[i,j] = fun(np.array([xx[i], yy[j]]))
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    surf = ax.plot_surface(X, Y, Z)
    if save is not None:
        plt.savefig(save)
    return fig

### penalizing funtions


def penalization(x, kcsub=None, eqInd=None, ineqInd=None, lb=None, ub=None, sigma=2):
    '''
    penalization(x, kcsub, eqInd=None, ineqInd=None, lb=None, ub=None, sigma=2):
    '''
    val = 0
    if kcsub is not None:
        if eqInd is not None:
            aux = []
            for k in eqInd:
                c, status = kcsub(x, k)
                if status != 0:
                    print('Error: constraint function returner status = {}\n'.format(status))
                    return 0, status
                if c > 0:
                    aux.append((c, 1, k))
                elif c < 0:
                    aux.append((-c, -1, k))
            if len(aux) > 0:
                h, signH, keys = zip(*aux)
                h = np.array(h)
                signH = np.array(signH)
            else:
                h = np.array([])
                signH = np.array([])
                keys = []
        else:
            h = np.array([])
            signH = np.array([])
            keys = []

        if ineqInd is not None:
            aux = []
            for k in ineqInd:
                c, status = kcsub(x, k)
                if status != 0:
                    print('Error: constraint function returner status = {}\n'.format(status))
                    return 0, status
                if c > 0:
                    aux.append((c, k))
            if len(aux) > 0:
                g, keys = zip(*aux)
                g = np.array(g)
            else:
                g = np.array([])
                keys = []
        else:
            g = np.array([])
            keys = []
    else:
        h = np.array([])
        signH = np.array([])
        keys = []
        g = np.array([])
        keys = []


    if lb is not None:
        ind = (lb>-np.inf) * (lb<np.inf)
        lbb = lb[ind] - x[ind]
        lbb = lbb[lbb > 0]
    else:
        lbb = np.array([])

    if ub is not None:
        ind = (ub>-np.inf) * (ub<np.inf)
        ubb =  x[ind] - ub[ind]
        ubb = ubb[ubb > 0]
    else:
        ubb = np.array([])
    v = np.concatenate([h,g,lbb,ubb])

    if sigma == 2:
        val += 0.5 * np.dot(v,v)
    else:
        val += np.sum(np.exp(sigma * np.log(v))) / sigma
    status = 0
    return val, 0

