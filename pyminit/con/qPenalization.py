'''
we consider the prlblme
min f(x)
lb<=x<=ub
h(x) =0
g(x)<=0
'''
import numpy as np
import scipy.optimize


def penalization(x, h=None, g=None, lb=None, ub=None):
    val = 0
    if h is not None:
        val += np.dot(h(x), h(x)) / 2
    if g is not None:
        aux = g(x)
        aux = aux[aux>=0]
        val += np.dot(aux, aux) / 2
    if lb is not None:
        ind = (lb>-np.inf) * (lb<np.inf)
        aux = lb[ind] - x[ind]
        aux = aux[aux>=0]
        val += np.dot(aux, aux) / 2
    if ub is not None:
        ind = (ub>-np.inf) * (ub<np.inf)
        aux =  x[ind] - ub[ind]
        aux = aux[aux>=0]
        val += np.dot(aux, aux) / 2
    return val

def penalizationJac(x, h=None, hJac=None, g=None, gJac=None, lb=None, ub=None):
    grad = np.zeros_like(x)
    if h is not None:
        grad += np.dot(hJac(x).T, h(x))
    if g is not None:
        aux = g(x)
        aux[aux<=0] = 0
        grad += np.dot(gJac(x).T, aux)
    if lb is not None:
        auxGrad = np.zeros_like(x)
        ind = (lb>-np.inf) * (lb<np.inf)
        aux = lb[ind] - x[ind]
        aux[aux<0] = 0
        auxGrad[ind] = -aux
        grad += auxGrad
    if ub is not None:
        auxGrad = np.zeros_like(x)
        ind = (ub>-np.inf) * (ub<np.inf)
        aux =  x[ind] - ub[ind]
        aux[aux < 0] = 0
        auxGrad[ind] = aux
        grad += auxGrad
    return grad

def quadPen(fun, x0, grad=None, h=None, hJac=None, g=None, gJac=None, lb=None, ub=None, maxIter=10, tol=1e-3, globalLog=None):

    if globalLog is not None:
        gLog = open(globalLog, 'w')
    pen = penalization(x0,h=h, g=g,lb=lb, ub=ub)
    iterCount = 0
    rPen = 5.0

    while (pen > tol) and (iterCount < maxIter):
        objFun = lambda x: fun(x) + rPen * penalization(x, h=h, g=g, lb=lb, ub=ub)
        objFunGrad = lambda x: grad(x) + rPen * penalizationJac(x, h=h, hJac=hJac, g=g, gJac=gJac, lb=lb, ub=ub)
        res = scipy.optimize.minimize(objFun, x0, jac=objFunGrad, method='L-BFGS-B', options={'maxiter':1000})
        iterCount += 1
        if globalLog is not None: gLog.write('iter {}:\n'.format(iterCount))
        if res['success']:
            x0 = res['x']
            rPen += 50
            pen = penalization(x0,h=h, g=g,lb=lb, ub=ub)
            if globalLog is not None: gLog.write('penalization: {}\n'.format(pen))
        else:
            print('sub problem could not be solved')
            iterCount = maxIter + 1
            print(res)
    if globalLog is not None: gLog.close()
    return x0, res['success']
