# Descent Direction
It is possíble to choose a `Descent Direction`  subroutine from the `DD` submodule or even program one such subroutine.
These subroutines can be chosen using the argument `dd` that can be a string or a `Descent Descent` function.

Currently we have available:
- 'gradient'
- 'bfgs' (It is important to note that this descent direction should be used  together with some line search that satisfies the curvature condition, i.e.  Wolfe line search)
- 'lm-bfgs (It is important to note that this descent direction should be used  together with some line search that satisfies the curvature condition, i.e.  Wolfe line search)

- 'conjugatedGradient' (The Cauchy exact line search is used by default)


```python
import numpy as np
import pyminit.unc.descent

def bb(x, mode=0, context=None):
	status = 0
	if mode == 0:
		val = (x[0] - 1) ** 2 + (np.dot(x,x) - 0.25) ** 2
		return val, status
	elif mode == 1:
		g = np.array([ 2 * (x[0] - 1) , 0]) + 4 * (np.dot(x,x) - 0.25) * x
		return g, status
	elif mode == 2:
		val = (x[0] - 1) ** 2 + (np.dot(x,x) - 0.25) ** 2
		g = np.array([ 2 * (x[0] - 1) , 0]) + 4 * (np.dot(x,x) - 0.25) * x
		return val, g, status
x0 = np.ones(2)
x, f, g, status = pyminit.unc.descent.minimize(x0, bb=bb, dd='gradient')
```

Also, you can program your own `Descent Direction` subroutine. This function must follow the parttern:
```python
def <name_descent_direction_routine>(x, g, context=None):
	x: numpy array
		current point
	g: numpy array
		gradient at x
return d, status
	d: numpy array
		descent direction
	status: int
		0 if success
```
For example:

```python
import numpy as np
def myDD(x,g, context=None):
	weights = np.aragen(1, len(x) + 1)
	status = 0
	return - weights * g, status

x0 = np.ones(2)
result = pyminit.unc.descent.minimize(x0, bb=bb, dd=myDD)
```
