import numpy as np
import pyminit.unc.DD
import pyminit.unc.LS
import pyminit.common as common

def descentM(x0, 
        bb, bbContext=None, 
        dd=None, ddContext=None, 
        ls=None, lsContext=None,
        xLog=None, globalLog=None, maxIter=100, tol=1e-4): 
    '''
    descentM(x0, 
        bb, bbContext=None, 
        dd=None, ddContext=None, 
        ls=None, lsContext=None,
        xLog=None, globalLog=None, maxIter=None)
        xLog=None, globalLog=None, maxIter=100, tol=1e-4)

    bb: black box
    x0: initial point
    dd: descent direction function
    ls: linear search method
    xLog: path file for recording the sequence rengerated by the algorithm
    globalLog: path file for general information 
    maxIter:
    '''
    import time
    startingTime = time.time()
    if xLog is not None: xLogFile = open(xLog, 'w')
    if globalLog is not None: globalLogFile = open(globalLog, 'w')

    if ((dd is None) and (ls is None)): #Setting BFGS
        # dd = pyminit.unc.DD.matrixBasedBFGSDirection
        # ddContext = {}
        dd = pyminit.unc.DD.minusGrad
        ddContext = None
        # ls = pyminit.unc.LS.WolfeLS
        ls = pyminit.unc.LS.ArmijoLS
        lsContext = None
    elif dd is None:# setting the default descent direction
        # dd = minusGrad
        dd = pyminit.unc.DD.minusGrad
        ddContext = None
    elif ls is None:
        # ls = constLS
        # ls = pyminit.unc.bfgs.Wolfe
        ls = pyminit.unc.LS.ArmijoLS
        lsContext = None
    

    fval, g, status = bb(x0, mode=2, context=bbContext)
    if status != 0: 
        print('error: Black returned status {}\n'.format(status))
        return None, None, None, status

    iterK = 0
    x = x0
    if xLog is not None: xLogFile.write( ','.join(['[k={}]'.format(iterK)] + [str(val) for val in x]) + '\n')

    gInfNorm = max(abs(g)) 

    while ((gInfNorm >  tol) and (iterK < maxIter)): 
        d, status = dd(x, g, ddContext)
        x, fval, g, status = ls(x, fval, g, d, bb,  bbContext=bbContext, context=lsContext)

        if status != 0: 
            print('error: Line Search returned status {}\n'.format(status))
            return None, None, None, status

        iterK +=1
        gInfNorm = max(abs(g)) 
    

        if xLog is not None: xLogFile.write( ','.join(['[k={}]'.format(iterK)] + [str(val) for val in x]) + '\n')
        if globalLog is not None: globalLogFile.write('[k={}], fval: {}, grad inf-norm: {}\n'.format(iterK, fval, gInfNorm))

    totalTime = time.time() - startingTime

    if globalLog is not None: 
        if ((iterK >= maxIter) and (gInfNorm >  tol)): 
            globalLogFile.write('Maximum number  of iteration reached without diminishing enough gradient norm\n')
        globalLogFile.write( 'Gradient Inf-Norm: {}\nFunction Value: {}\n Time: {}'.format(max(abs(g)), fval, totalTime))

    if xLog is not None: xLogFile.close()
    if globalLog is not None: globalLogFile.close()
    
    return common.result_class(status=status, gradInfNorm=max(abs(g)), x_last=x, f_last=fval, g_last=g, nIter=iterK, time=totalTime)

def minimize(x0,
        fun=None, grad=None, 
        bb=None, bbContext=None,
        dd=None, ddContext=None, 
        ls=None, lsContext=None,
        xLog=None, globalLog=None, maxIter=100, tol=1e-4): 
    '''
    min(x0,
        fun=None, grad=None, 
        bb=None, bbContext=None,
        dd=None, ddContext=None, 
        ls=None, lsContext=None,
        xLog=None, globalLog=None, maxIter=100, tol=1e-4)

    At least fun or bb must be provided.
    '''
    import pyminit.tools
    if bb is None:
        if fun is None:
            print('Error: Objective function is required')
            return None
        elif grad is None: 
                bb = lambda x,mode=0, context=None: pyminit.tools.numBB(x, mode=mode, context=context, fun=fun)
        else: 
            bb = lambda x,mode=0, context=None: pyminit.tools.fgBB(x, mode=mode, context=context, fun=fun, grad=grad)
    # default values for line search
    if isinstance(ls, str):
        if 'constant' in ls:
            ls = pyminit.unc.LS.constLS
        elif 'armijo' in ls:
            ls = pyminit.unc.LS.ArmijoLS
        elif 'wolfe'in ls:
            ls = pyminit.unc.LS.WolfeLS
        elif 'cauchy'in ls:
            ls = pyminit.unc.LS.CauchyLS
        else:
            print('{} line search is not available. Using default linear search\n'.format(ls))
            ls = None
    # default values for descent direction
    if isinstance(dd, str):
        if 'gradient' in dd:
            dd = pyminit.unc.DD.minusGrad 
        elif 'bfgs' in dd: 
            dd = pyminit.unc.DD.matrixBasedBFGSDirection
            if ddContext is None:
                ddContext = {}
            if ls is None:
                ls = pyminit.unc.LS.WolfeLS

        elif 'lm-bfgs' in dd: 
            dd = pyminit.unc.DD.limitedMemoryBFGS
            if ddContext is None:
                ddContext = {}
            if ls is None:
                ls = pyminit.unc.LS.WolfeLS
        elif 'conjugatedGradient' in dd: 
            dd = pyminit.unc.DD.conjugatedGradient
            print('Usning Conjugted Gradient. Cauchy Exact line search will be used\n')
            ls = pyminit.unc.LS.CauchyLS
            if ddContext is None:
                ddContext = {}
            if lsContext is None:
                lsContext = {}
        else:
            print('{} descent direction is not available. Using default descent direction\n'.format(dd))
            dd = None

    result = descentM(x0, bb, bbContext=bbContext, dd=dd, ddContext=ddContext, ls=ls, lsContext=lsContext, xLog=xLog, globalLog=globalLog, maxIter=maxIter, tol=tol)
    return result
