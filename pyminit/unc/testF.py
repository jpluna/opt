import numpy as np
'''
This submodule provides some function test:
    - rosenbrock (dimension 2)
    - sugar (dimension n)
    - quad (dimension n, context: positive definete matrix)
    - perturbedQuad (dimension n)
'''
def rosenbrock(x, mode=0, context=None):
    '''
    f(x) = sum_{i=1}^{n-1} 100(x_{i+1} - x_i^2)^2 +(1-x_i)^2
    the dimension is deduced from x
    '''
    status = 0
    n = len(x)
    if mode == 0:
        if (context is not None) and ('fun' in context): context['fun'] += 1
        return   100 * sum((x[1:] - (x[:-1] ** 2)) ** 2)  + sum((1 - x[:-1]) ** 2), status
    elif mode == 1:
        if (context is not None) and ('grad' in context): context['grad'] += 1
        g = np.zeros(n)
        for i in range(n-1):
            g[i] += 400 * x[i] * ((x[i] ** 2) - x[i + 1]) + (2 * (x[i] - 1))
            g[i + 1] += 200 * (x[i + 1]  - (x[i] ** 2)) 
        return g, status
    elif mode == 2:
        fval =  100 * sum((x[1:] - (x[:-1] ** 2)) ** 2)  + sum((1 - x[:-1]) ** 2)
        g = np.zeros(n)
        for i in range(n-1):
            g[i] += 400 * x[i] * ((x[i] ** 2) - x[i + 1]) + (2 * (x[i] - 1))
            g[i + 1] += 200 * (x[i + 1]  - (x[i] ** 2)) 
        if (context is not None) and ('fun' in context): context['fun'] += 1
        if (context is not None) and ('grad' in context): context['grad'] += 1

        return fval, g, status


def sugar(x, mode=0, context={}): 
    status = 0
    if mode == 0:
        return x.sum() * np.exp(-6.0 * np.dot(x,x)), 0
    elif mode == 1: 
        return np.exp(-6.0*np.dot(x,x))*(np.ones_like(x) - 12 * x.sum() * x), 0
    elif mode == 2:
        return x.sum() * np.exp(-6.0 * np.dot(x, x)), np.exp(-6.0*np.dot(x,x))*(np.ones_like(x) - 12 * x.sum() * x), 0
    elif mode == 3:
        aux = -12 * np.exp(-6.0*np.dot(x,x)) * x
        aux1 = np.ones_like(x) - 12 * x.sum() * x
        ## aux2 = grad(aux1)
        aux2 = -12 * (np.dot(x.reshape(len(x),1),np.ones_like(x).reshape(1,len(x))) + x.sum() * np.eye(len(x)))
        return np.dot(aux1.reshape(len(aux1), 1), aux.reshape(1,len(aux))) + np.exp(-6.0*np.dot(x,x)) * aux2, 0

def quad(x, mode=0, context=None):
    if context is not None:
        H = context
    else: 
        H = np.eye(len(x))
    if mode == 0:
        return 0.5 * np.dot(np.dot(H,x), x), 0
    elif mode ==1:
        return np.dot(H,x), 0
    elif mode == 2:
        return 0.5 * np.dot(np.dot(H,x), x), np.dot(H,x), 0
    elif mode == 3:
        return H, 0
        
def perturbedQuad(x, mode=0, context=None):
    '''
    $f(x) =\sum_{i=1}^n i x_i^2 +\frac{1}{100}(\sum_{i=1}^n x_i)^2 $
    '''
    n = len(x)
    I = np.arange(1, n + 1)
    if mode == 0:
        return np.dot(I, x ** 2) + (((x.sum()) ** 2) / 100), 0
    elif mode == 1:
        return 2 * I * x + (x.sum() * np.ones(n) / 50), 0 
    elif mode == 2:
        return np.dot(I, x ** 2) + (((x.sum()) ** 2) / 100), 2 * I * x + (x.sum() * np.ones(n) / 50), 0 

