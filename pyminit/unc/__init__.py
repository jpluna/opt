'''
The unconstrained (smooth) optimization module. It has the following submodules:
- LS: Line Search module routines
- DD: Descent direction routines
- descent: minimization routines
'''

__all__=['testF', 'descent', 'LS', 'DD']

from . import *
