'''
Descent Direction  Module
=========================
This module provides some Descent Direction routines
matrixBasedBFGSDirection
minusGrad
Any method follows the same pattern: 
    def (x, g, context=None):
        x: numpy array
            current point
        g: numpy array
            gradient at x
    return d, status
        d: numpy array
            descent direction
        status: int
            0 if success
'''
import numpy as np

def minusGrad(x, g, context=None):
    '''
    x: current point
    g: gradient at current point
    context: some data requiered for the function
    '''
    d = -g
    status = 0
    return d, status

### matrix based bfgs direction
def matrixBasedBFGSDirection(x, g, context):
    n = len(x)
    status = 0
    if 'W' not in context: context['W'] = np.eye(n)

    if 'xp' in context:
        s = x - context['xp']
        y = g - context['gp']
        sy = np.dot(s,y)
        if sy <= 0 :
            # error, it is not possible to genrate a descent direction
            status = 1
            d = np.zeros(n)
        else: 
            context['W'] = np.dot(context['W'], np.eye(n) - np.dot(np.array([y]), np.array([s]).T)/sy)
            context['W'] = np.dot( np.eye(n) - np.dot(np.array([s]), np.array([y]).T)/sy, context['W'])
            context['W'] += np.dot(np.array([s]), np.array([s]).T)/sy
            context['xp'] = np.array(x)
            context['gp'] = np.array(g)
    d = -np.dot(context['W'], g)
    return d, status


def limitedMemoryBFGS(x, g, context):
    status = 0
    if 'size' not in context: context['size'] = 10
    if '_n' not in context:
        n = len(x)
        context['_n'] = n
    else:
        n = context['_n']
    if '_memoryIsFull' not in context: context['_memoryIsFull'] = False
    if '_memory' not in context: context['_memory']= []
    if len(context['_memory']) > 0:
        context['_memory'][-1][0] = x - context['_memory'][-1][0]
        context['_memory'][-1][1] = g - context['_memory'][-1][1]
        context['_memory'][-1][2] = np.dot(context['_memory'][-1][0], context['_memory'][-1][1])
    q = g
    alpha = []
    for s, y, sy in context['_memory'][::-1]:
        aa = np.dot(q, s)
        q = q - aa * y
        alpha.append(aa)
    alpha = alpha[::-1]
    h = q
    for aa, (s, y, sy) in zip(alpha, context['_memory']):
        beta = np.dot(h,y) / sy
        h = h + (aa - beta) * s
    d = -h
    # updating memory
    if context['_memoryIsFull'] and (context['size']>0):
        del context['_memory'][0] # deleting (forgeting) the oldest tuple (s, y, sy)
    elif ((len(context['_memory']) == (context['size'] -1)) or (context['size'] == 0)):
        context['_memoryIsFull'] = True

    if context['size'] > 0: 
        context['_memory'].append((x, g, 0)) # storing the point (x, g) for computing the next pair (s, y ,sy)
    return d, status

def conjugatedGradient(x, g, context):
    status = 0
    g_square  = np.dot(g,g)
    if 'd_prev' not in context:
        d = -g
    else:
        ck = - g_square / context['d_prev'][0][1]
        d = -g  + ck * context['d_prev'][0][0]
    context['d_prev']=[(d, g_square)]
    return d, status
