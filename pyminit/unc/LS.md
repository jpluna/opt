## Line Search Subroutine


The submodue pyminit.unc.LS offers some line search subroutines
- 'constant' 
- 'armijo' 
- 'wolfe'
- 'cauchy'

all these methods admit a context argument `lsContext` used for passing information for 
controling it behavior. The default is  `lsContext=None` which makes the algorithm to 
use its default configuration.

We can choose the line search method name via the argument `ls` in  `pyminit.unc.descent.minimize`. 
```python
import numpy as np
import pyminit.unc.descent

def bb(x, mode=0, context=None):
	status = 0
	if mode == 0:
		val = (x[0] - 1) ** 2 + (np.dot(x,x) - 0.25) ** 2
		return val, status
	elif mode == 1:
		g = np.array([ 2 * (x[0] - 1) , 0]) + 4 * (np.dot(x,x) - 0.25) * x
		return g, status
	elif mode == 2:
		val = (x[0] - 1) ** 2 + (np.dot(x,x) - 0.25) ** 2
		g = np.array([ 2 * (x[0] - 1) , 0]) + 4 * (np.dot(x,x) - 0.25) * x
		return val, g, status
x0 = np.ones(2)
ls = 'armijo'
lsContext = None
result = pyminit.unc.descent.minimize(x0, bb=bb, ls=ls, lsContext=lsContext)
```

# Programming your own line search method.
Additionally, it is possible to code a own routine.  This routine should follow the parttern:
```python
def  <subroutine_name>(xk, fk, gk, dk, bb,  bbContext=None, context=None):
	[code]
    return xNext, fNext, gNext, status

where the input paramenters are:
	xk:  np.array. The point at which the line search will be performed.
	fk, gk: float, np.array. The value and gradient  of the objective function at xk.
	dk: np.array. The descent direction.
	bb: <functin>.the objective function Black-Box subroutine
	bbContext: the `bb` context.
	context: the context to be used for controling the behavior of the line search routine.

the output of the routine is:
	xNext: np.array. The next point after computing the stepsize. that is xNext = xk + tk*dk, where tk is 
		the stepsize computed.
	fNext, gNext: float, np.array. The value and gradient of the objective function at xNext.
	status: int. 0 if the routine performed properly, otherwise, it should return any other number.
```

For example, we can program a line search that at its $`k`$-th call uses the stepsize $`1/k`$. The number 
of calls will be stored in a dictionary `{'calls': 0}` that will passed as the `context` of the method.

```python
def  my_line_search_routine(xk, fk, gk, dk, bb,  bbContext=None, context=None):
	context['calls'] += 1
	tk = 1.0 / context['calls']
	xNext = xk + tk * dk
	fNext, gNext, status = bb(xNext, mode=2, context = bbContext)
    return xNext, fNext, gNext, status

def bb(x, mode=0, context=None):
	status = 0
	if mode == 0:
		val = (x[0] - 1) ** 2 + (np.dot(x,x) - 0.25) ** 2
		return val, status
	elif mode == 1:
		g = np.array([ 2 * (x[0] - 1) , 0]) + 4 * (np.dot(x,x) - 0.25) * x
		return g, status
	elif mode == 2:
		val = (x[0] - 1) ** 2 + (np.dot(x,x) - 0.25) ** 2
		g = np.array([ 2 * (x[0] - 1) , 0]) + 4 * (np.dot(x,x) - 0.25) * x
		return val, g, status


ls = my_line_search_routine
lsContext = {'calls': 0}
x0 = np.ones(2)
result = pyminit.unc.descent.minimize(x0, bb=bb, ls=ls, lsContext=lsContext)
```
