'''
Line Search Module
==================
- This module provides some Line Search routines:
- constLS
- ArmijoLS
- WolfeLS

Any method follows the same pattern: 
    def  <subroutine_name>(xk, fk, gk, dk, bb,  bbContext=None, context=None):
    return xNext, fNext, gNext, status
'''
import numpy as np
### Interpolation
def bisectionInter(L, R, fL=None,  gL=None,  fR=None, context=None):
    status = 0
    if context is None:
        factor = 0.5
    else:
        factor = context['factor']
    return  factor * L + (1 - factor) * R, status

##### extrapolation
def powerExtra(L, fL=None,  gL=None, context=None):
    status = 0
    factor = 2
    if (context is not None) and ('factor' in context):
        factor = context['factor']
    return  factor * L, status


### Line Search
def constLS(x_c, f_c, g_c, d, bb,  bbContext=None, context=None):
    '''
    x_c, f_c, g_c: current point, function value and gradient, respectively
    d: descent direction
    bb: blackbox
    bbContext: blackBox context
    context: float
        the stepsize. If None, it is used 0.01
    '''
    if context is None:
        stepSize = 0.01
    else:
        stepSize = context

    xNext = x_c + stepSize * d
    fNext, gNext, statusBB = bb(xNext, mode=2, context=bbContext)
    return xNext, fNext, gNext, statusBB

### Armijo line search

def ArmijoLS(xk, fk, gk, dk, bb,  bbContext=None, context=None):
    '''
    xk, fk, gk: current point, function value and gradient, respectively
    dk: descent direction
    bb: blackbox
    bbContext: blackBox context
    context: dict
        ArmijoLS context. It may have any/all of  the following fields:
            - interFun: function for made interpolations
            - interContext: context for 'interFun'
            - sigmaA: decrease parameter
            - maxIter: maximum number of iteration
            - stepSize: Initial stepSize
        '''
    # setting default parameters values
    interFun = bisectionInter
    interContext = None
    sigmaA = 0.5
    maxIter = 50
    stepSize = 10

    # updating parameters values from context
    if (context is not None):
        if 'interFun' in context: interFun = context['interFun']
        if 'interContext' in context: interContext = context['interContext']
        if 'sigmaA' in context: sigmaA = context['sigmaA']
        if 'maxIter' in context: maxIter = context['maxIter']
        if 'stepSize' in context: stepSize = context['stepSize']

    iterCount = 0
    sigmaAgkdk = sigmaA * np.dot(gk, dk)
    L = 0

    xNext = xk + stepSize * dk
    fNext, bbStatus = bb(xNext, mode=0, context=bbContext)
    ArmijoOK = (fNext <= fk + stepSize * sigmaAgkdk)

    while (not ArmijoOK) and (iterCount < maxIter): 
        iterCount +=1
        R = stepSize
        stepSize, interFunStatus = interFun(L, R, fL=fk, fR=fNext, gL=gk, context=interContext)
        xNext = xk + stepSize * dk
        fNext, bbStatus = bb(xNext, mode=0, context=bbContext)
        ArmijoOK = (fNext <= fk + stepSize * sigmaAgkdk)

    gNext, bbStatus = bb(xNext, mode=1, context=bbContext)
    return xNext, fNext, gNext, bbStatus

####### Wolfe Linear Search

def WolfeLS(xk, fk, gk, dk, bb,  bbContext=None, context=None):
    '''
    x_c, f_c, g_c: current point, function value and gradient, respectively
    d: descent direction
    bb: blackbox
    context: dict
        WolfeLS context. It may have any/all of  the following fields: 
            - interFun: function for made interpolations
            - extrapFun: function for made extrapolation
            - interContext: context for 'interFun'
            - extrapContext: context for 'interFun'
            - sigmaA: decrease parameter
            - sigmaC :  Curvature parameter
            - maxIter: maximum number of iteration
            - stepSize: Initial stepSize
            - debugMessage: True/False Default: False
    '''
    if 'debugMessage' in context:
        debugMessage = context['debugMessage']
    else: 
        debugMessage = False #Just to show information of the line search for debuggging
    ## Setting default parameters
    interFun = bisectionInter
    extrapFun = powerExtra
    interContext = None
    extrapContext = None
    sigmaA = 0.2 # Armijo parameter
    sigmaC = 0.8 # Curvature parameter
    maxIter = 50
    stepSize = 0.1

    # updating parameters values from context
    if (context is not None):
        if 'interFun' in context: interFun = context['interFun']
        if 'extrapFun' in context: extrapFun = context['extrapFun']
        if 'interContext' in context: interContext = context['interContext']
        if 'extrapContext' in context: extrapContext = context['extrapContext']
        if 'sigmaA' in context: sigmaA = context['sigmaA']
        if 'sigmaC' in context: sigmaC = context['sigmaC']
        if 'maxIter' in context: maxIter = context['maxIter']
        if 'stepSize' in context: stepSize = context['stepSize']

    iterCount = 0

    t = stepSize # initial step

    wolfeOK = False
    L = 0
    fL = fk
    gL = np.array(gk)
    R = 0
    sigmaAdg = sigmaA * np.dot(dk, gk)
    sigmaCdg = sigmaC * sigmaAdg / sigmaA
    x = np.array(xk)
    gx = None
    while ((not wolfeOK) and (iterCount < maxIter)):
        iterCount += 1
        if debugMessage: print('[k={}],[t={}][{} -- {}]'.format(iterCount,t,L,R))
        x = xk + t * dk
        fx, status = bb(x, mode=0, context=bbContext)
        # Checking Armijo condition
        armigo = (fx <= fk + t * sigmaAdg)
        if debugMessage: print('sigmaAdg={}'.format(sigmaAdg))
        if debugMessage: print('[k={}],[t={}][fk={} -- fx={}]'.format(iterCount,t,fk,fx))
        if armigo:
            if debugMessage: print('[k={}],[t={}]Armijo OK'.format(iterCount,t))
            # Armijo OK
            # Checking curvature condition
            gx, status = bb(x, mode=1, context=bbContext)
            curvatureCondition = (sigmaCdg <= np.dot(gx, dk))
            if curvatureCondition:
                wolfeOK = True
                if debugMessage: print('[k={}],[t={}] Wolfe OK'.format(iterCount, t))
                return x, fx, gx, status
            else:
                if debugMessage: print('[k={}],[t={}] Not curvatureCondition'.format(iterCount, t))
                # Armijo OK, Curvature not
                # to amall step
                L = t
                fL = fx
                gL = gx
                if R == 0:
                    # there are not yet right (large) steps
                    # extrapolation is required
                    if debugMessage: print('extrapolation')
                    t, status = extrapFun(L, fL=fL,  gL=gL, context=extrapContext)
                else:
                    # there is already righ bound R
                    # we interpolate
                    print('intrapolation')
                    t, status = interFun(L, R, fL=fL, gL=gL, fR=fR, context=interContext)                   
        else:
            if debugMessage: print('[k={}],[t={}] Not armijo'.format(iterCount, t))
            #  Armijo condition is not satisfied
            # too large step
            R = t
            fR = fx
            # we interpolate
            t, status = interFun(L, R, fL=fL, gL=gL, fR=fR,  context=interContext)                   
            if debugMessage: print('intrapolation')

    if iterCount == maxIter: 
        print('Wolfe Line Search: maximum number of iterations ({}) reacherd\n'.format(iterCount))
        status = 1

    return x, fx, gx, status


### Cauchy exact search
def CauchyLS(x_c, f_c, g_c, d, bb,  bbContext=None, context=None):
    status = 0
    if 'g0' not in context:
        g0, status = bb(0*x_c, mode=1, context=bbContext)
        context['g0'] = g0
    else:
        g0 = context['g0']

    gd, status = bb(d, mode=1, context=bbContext)
    tk  = - np.dot(g_c, d) / np.dot(gd-g0,d)
    
    x = x_c + tk * d
    fx, gx, status = bb(x, mode=2, context=bbContext)
    return x, fx, gx, status
