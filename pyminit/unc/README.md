# unc
The unconstrained (smooth) optimization module. 

This module offers a highly configurable descent method for minimizing nonlinear funtions. 

The easiest way of using this methods is by using pyminint.unc.descent.minimize  which after 
minimizing the problem returns a `result` object that has some result information
- `x_last`: Last point found by the algorithm.
- `f_last`: Value at `x_last'
- `g_last`: The gradient at `x_last`
- `gradInfNorm`: The inf norm of `g_last`
- `status`: 0 if algorithm ended with optimality.
- `nIter`: Number of iterations performed
- `time`: time in seconds used by the algorithm

### Example
For minimizing $`f(x) = (x_1 -1)^2 + (x_1^2 + x_2^2-0.25)^2 `$

```python
import numpy as np
import pyminit.unc.descent

def fun(x):
	val = (x[0] - 1) ** 2 + (np.dot(x,x) - 0.25) ** 2
	return val
x0 = np.ones(2)
result = pyminit.unc.descent.minimize(x0, fun=fun)
print(result)
```

```python
f_last = 0.1472204060462985
gradInfNorm = 0.000898594071652159
nIter = 100
status = 0
time = 0.03232908248901367
```

You can also supply the function that computer the gradient of the objective function


```python
def grad(x):
	g = np.array([ 2 * (x[0] - 1) , 0]) + 4 * (np.dot(x,x) - 0.25) * x
	return g
x0 = np.ones(2)
result = pyminit.unc.descent.minimize(x0, fun=fun, grad=grad)
```
Also there is possible to describe  the function to be minimized via a Black-Box routine.

```python
def bb(x, mode=0, context=None):
	status = 0
	if mode == 0:
		val = (x[0] - 1) ** 2 + (np.dot(x,x) - 0.25) ** 2
		return val, status
	elif mode == 1:
		g = np.array([ 2 * (x[0] - 1) , 0]) + 4 * (np.dot(x,x) - 0.25) * x
		return g, status
	elif mode == 2:
		val = (x[0] - 1) ** 2 + (np.dot(x,x) - 0.25) ** 2
		g = np.array([ 2 * (x[0] - 1) , 0]) + 4 * (np.dot(x,x) - 0.25) * x
		return val, g, status
x0 = np.ones(2)
result = pyminit.unc.descent.minimize(x0, bb=bb)
		
```
The minimizing algorithm used by default is the well known **BFGS algorithm**.

It is possible to change the behavior of the algorihtm by changing and/or defining the [line search](./LS.md) and/or
[descent direction](./DD.md) subroutines.
