class result_class():
    time=None
    x_best=None
    f_best=None
    x_last=None
    g_last=None
    f_last=None
    deltak=None
    gradInfNorm=None
    nIter=None
    status=None
    def __init__(self, x_best=None, f_best=None, x_last=None, f_last=None, deltak=None, nIter=None,status=None, time=None, gradInfNorm=None, g_last=None):
        self.x_best=x_best
        self.f_best=f_best
        self.x_last=x_last
        self.g_last=g_last
        self.f_last=f_last
        self.gradInfNorm=gradInfNorm
        self.deltak=deltak
        self.nIter=nIter
        self.status=status
        self.time=time
    def __repr__(self):
           return  '\n'.join(['{} = {}'.format(key, val) for key, val in self.__dict__.items() if (('x_' not in key) and ('g_' not in key) and (val is not None))])

