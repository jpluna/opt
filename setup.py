import setuptools
from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
from Cython.Distutils import build_ext
import platform
import os
import numpy
# os.environ['CC']='gcc-8'
osy =  platform.system() 
# if osy == 'Linux': 
    # libalgencanPath = './algencanLib/linux/libalgencan.a'
# elif osy == 'Darwin':
    # libalgencanPath = './algencanLib/osx/libalgencan.a'
# minitDir = '/Users/juanpa/gitLab/opt/minit'

minitDir = './minit'
vectorOp = os.path.join(minitDir, 'extraLib/cBasicLinAlg/source/vectorOperations.c')
vectorOpH = os.path.join(minitDir, 'extraLib/cBasicLinAlg/include')
proxB = os.path.join(minitDir, 'ns/proxBundle/source/proxBundle.c')
proxSolver = os.path.join(minitDir, 'ns/proxBundle/source/proxSolver.c')
bbToolsInc = os.path.join(minitDir, 'include')
proxBH = os.path.join(minitDir, 'ns/proxBundle/include')
sourcefilesProxB = [
        'pyminit/ns/cProxBundle.pyx',
        # './pyminit/ns/*.pyx',
        vectorOp,
        proxB,
        proxSolver,
        ]
include_dirsProxB = [ 
        numpy.get_include(),
        vectorOpH,
        bbToolsInc,
        proxBH,
        ".",
        ]

sourcefilesBMRIO = [
        'pyminit/ns/cbmrio.pyx',
        # './pyminit/ns/*.pyx',
        ]


if osy == 'Linux': 
    libbmrioPath = os.path.join(minitDir, 'ns/bmrio/lib/linux')
    bmrioLib = ['gfortran']
elif osy == 'Darwin':
    libbmrioPath = os.path.join(minitDir, 'ns/bmrio/lib/osx')
    bmrioLib = [ 'm' ]

include_dirsBMRIO = [ 
        os.path.join(minitDir, 'ns/bmrio/include'),
        ".",
         numpy.get_include(),
        ]

extensions = [
        Extension("pyminit.ns.cProxBundle", 
            sourcefilesProxB,
            include_dirs=include_dirsProxB,
            libraries=['gurobi80'],
            # libraries=['gfortran', 'm'],
            # libraries=['gfortran'],
            # library_dirs=['/usr/local/Cellar/gcc/8.3.0_2/lib/gcc/8/'],
            # extra_objects=['./algencanLib/linux/libalgencan.a']
            # extra_link_args=["-lm -lgfortran"]
             # extra_link_args=[" -lgurobi80"]
            ), 
        Extension("pyminit.ns.cbmrio", 
            sourcefilesBMRIO,
            include_dirs=include_dirsBMRIO,
            # libraries=['bmrio'],
            # libraries=['gfortran', 'm'],
            # libraries=bmrioLib,
            libraries=['m', 'bmrio'],
            library_dirs=[libbmrioPath],
            # extra_objects=[os.path.join(libbmrioPath,'bmrio.a')],
            # extra_objects=['./algencanLib/linux/libalgencan.a']
            # extra_objects=[libalgencanPath],
            # extra_link_args=["-lm -lgfortran"]
             # extra_link_args=[" -lgurobi80"]
            ),
        ]

setup(
    name='PyMinIt',

    # Versions should comply with PEP440.  For a discussion on single-sourcing
    # the version across setup.py and the project code, see
    # https://packaging.python.org/en/latest/single_source_version.html
    version='0.0.1.dev2',

    description='Optimization algorithms',
    long_description='optimizaiton algorithms',

    # The project's main homepage.
    # url='https://github.com/pypa/sampleproject',

    # Author details
    author='Juan Pablo Luna',
    author_email='jpluna@po.coppe.ufrj.br',

    # Choose your license
    license='MIT',

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',

        # Indicate who your project is intended for
        'Intended Audience :: mathematical programming practitioners',
        'Topic :: Software :: Solvers ',

        # Pick your license as you wish (should match "license" above)
        'License :: OSI Approved :: MIT License',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        'Programming Language :: Python :: 3.5',
    ],

    # What does your project relate to?
    keywords='optimization, algorithms, minimization ',

    # You can just specify the packages manually here if your project is
    # simple. Or you can use find_packages().
    # packages=find_packages(exclude=['contrib', 'docs', 'tests']),
    packages=['pyminit', 'pyminit.ns','pyminit.unc',],
    cmdclass = {'build_ext': build_ext},
    ext_modules=cythonize(extensions),
    # package_dir = { 'pyminit.ns': 'pyminit/ns'},

    # Alternatively, if you want to distribute just a my_module.py, uncomment
    # this: 
    # py_modules=["pyminit.ns.cProxBundle"],

    # List run-time dependencies here.  These will be installed by pip when
    # your project is installed. For an analysis of "install_requires" vs pip's
    # requirements files see:
    # https://packaging.python.org/en/latest/requirements.html
    install_requires=['matplotlib', 'numpy', 'scipy', 'pandas', 'cython'],

    # List additional groups of dependencies here (e.g. development
    # dependencies). You can install these using the following syntax,
    # for example:
    # $ pip install -e .[dev,test]
    extras_require={
        'dev': ['check-manifest'],
        'test': ['coverage'],
    },

    # If there are data files included in your packages that need to be
    # installed, specify them here.  If using Python 2.6 or less, then these
    # have to be included in MANIFEST.in as well.
#     package_data={
        # 'sample': ['package_data.dat'],
    # },

#     package_data={
            # 'mathp':['path/lib/linux64/path.so', 'path/pathlib/lib/linux64/libpath47.so', 
                # 'path/C/lib/linux64/libgeneralMCP.so',
                # ] 
            # },
    include_package_data=True,
    # distclass=BinaryDistribution,
    # Although 'package_data' is the preferred approach, in some case you may
    # need to place data files outside of your packages. See:
    # http://docs.python.org/3.4/distutils/setupscript.html#installing-additional-files # noqa
    # In this case, 'data_file' will be installed into '<sys.prefix>/my_data'
    # data_files=[('my_data', ['data/data_file'])],
    data_files=[],

    # To provide executable scripts, use entry points in preference to the
    # "scripts" keyword. Entry points provide cross-platform support and allow
    # pip to create the appropriate form of executable for the target platform.
    entry_points={
        'console_scripts': []
        }
)
