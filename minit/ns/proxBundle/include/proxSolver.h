#ifndef PROXSOLVER_H
#define PROXSOLVER_H
#include<stdlib.h>
#include<blackBoxTools.h>
#include<proxBundle.h>
#include<gurobi_c.h>

typedef struct { 
	GRBenv *env; 
	GRBmodel *model; 
}gurobiModelContext;

int solveProxLMDual(double *xk, const int n, double *fk, proxBundle *bundle, double *lb, double *ub,
		int num_lcon,int *conSize, int **colIndex, double **lhs, int *conSense, double * rhs,
		double *muk, bundleManager * bundleManagerFun, void * bundleManagerContext, 
		double *sa,  double *zkprox, double *proxOptVal,
		double *deltak, double *c0a, void *context);
/*
	input:
	====== 
		xk: the proximal center. 
		n: the dimension fo the probem
		fk: the function value at the prox center.
		bundle: the bundle 
		lb, ub: bound of the decision variable
		muk: proximal parameter 
		 bundleManagerFun, bundleManagerContext: bundle manager function and its context
	output:
	=======
		sa: agregated subgradient 
		zkprox: solution of the approximated proximal problem 
		proxOptVal: optimal value of the approximated proximal problem 
		deltak: gap
		c0a: constant term of the agregated line. c0a = fk -sa*xk - ea
		*/

int solveProxLMPrimal(double *xk, const int n, double *fk, proxBundle *bundle, double *lb, double *ub,
		int num_lcon,int *conSize, int **colIndex, double **lhs, int *conSense, double * rhs,
		double *muk, bundleManager * bundleManagerFun, void * bundleManagerContext, 
		double *sa,  double *zkprox, double *proxOptVal,
		double *deltak, double *c0a, void *context);

void *  initProxMDual2Context();
// returns a context with  env=NULL and model=NULL

int solveProxLMDual2(double *xk, const int n, double *fk, proxBundle *bundle, double *lb, double *ub,
		int num_lcon,int *conSize, int **colIndex, double **lhs, int *conSense, double * rhs,
		double *muk, bundleManager * bundleManagerFun, void * bundleManagerContext, 
		double *sa,  double *zkprox, double *proxOptVal,
		double *deltak, double *c0a, void *context);



#endif
