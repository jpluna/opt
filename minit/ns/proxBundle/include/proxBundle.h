#ifndef PROXBUNDLE_H
#define PROXBUNDLE_H
#include<stdlib.h>
#include<blackBoxTools.h>
typedef struct proxBundle_
{
	// each cut: L(x) = <c1, x> + c0
	int maxSize;
	int size;
	int absLastId;//Newest cut id absulute value
	int **cut;// cut[i] = [idcut, position]. ***IMPORTANT*** the columns [:,1] (second columns) always must contain a permutation of [0,1,..maxSize-1]
	double **c1;// c1[position]
	double  *c0;// c0[position] 
	double  *alpha;// alpha[position] mulitplier associated to the cut
} proxBundle;
//all ids in the bundle always should be sorted from old to new.

typedef struct{
	int n;
	int *nIter;
	int seriousStepPerformed;
	double *x_serious;
	double *f_serious;
	double *x_last;
	double *f_last;
	double *delta;
}algorithmState;


typedef int stopCriterion(double *sa, const int n,  const double muk, const double tol, algorithmState *state, void *stopCriterionContext);


typedef int bundleManager(proxBundle *bundle, void* context);

typedef int proxLMSolver(double *xk, const int n, double *fk, 
		proxBundle *bundle, 
		double *lb, double *ub,
		int num_lcon,int *conSize, int **colIndex, double **lhs, int *conSense, double * rhs,
		double *muk, bundleManager * bundleManagerFun, void * bundleManagerContext, 
		double *sa,  double *zkprox, double *proxOptVal,
		double *deltak, double *c0a,
		void *context);


typedef double proxParamManager(int n, double *xk, double *sk, double *zkprox, double *skprox, int iterK, int changeProxCenter, void * context);


proxBundle * initProxBundle(int maxSize, int n);
//maxSize: max length of the bundle
//n: dimension of the problem
//
int maxSizeBundleManager(proxBundle *bundle, void* context);

int nonZeroMultiplierBundleManager(proxBundle *bundle, void* context);




int proxBundleMethod(double *x0, int n,  
		blackBoxP bb, void *bbContext, 
		proxLMSolver *solveProxM, void *solveProxMConxtext,
		proxParamManager *proxParamManagerFun, void * proxParamContext,
		bundleManager *bundleManagement, void * bundleManagementContext,
		proxBundle *bundle,
        double m,
		double *lb, double *ub,
		int num_lcon,int *conSize, int **colIndex, double **lhs, int *conSense, double * rhs,
        void *context, 
		char *xLog, char *globalLog, 
		stopCriterion *stopCriterionFun, void *stopCriterionContext,
        double tol, int maxIter,
		double *x_best, double *f_best,double * x_last, double *f_last, 
		double *deltak, int * nIter, double *time);
/*
 * input:
 * ======
 * x0: initial point
 * n: dimension
 * bb: blackBox
 * bbContext:
 * solveProxM: quadratic model solver 
 * solveProxMConxtext:
 * proxParamManagerFun: function that manages the proximal parameter. for interK=0 should return thei nitial prox parameter
 * proxParamContext:
 * bundleManagement:
 * bundleManagementContext:
 * bundle:
 * m: in <0,1>; parameter for checking serios steps
 * lb, ub: bounds
 * context: method context
 * xLog, globalLog: output files names
 * stopCriterionFun: 
 * tol: tolerance
 * maxIter:
 *
 * output:
 * =======
 * status: 0 if OK.
 * iter: number of iterations
 * x_best: 
 * f_best:
 * x_last:
 * f_last:
 * time:
 *
 */


int proxBundleMethodMin(double *x0, int n,  
		blackBoxP bb, void *bbContext, 
		proxLMSolver *solveProxM, void *solveProxMConxtext,
        /*proxParam=None, proxParamContext=None,*/
        /*bundleManagement=None, bundleManagementContext=None,*/
		bundleManager *bundleManagerFun, void * bundleManagerContext,
		proxBundle *bundle,
		stopCriterion *stopCriterionFun,
		void *stopCriterionContext,
        double m,
		double *lb, double *ub,
		int num_lcon,int *conSize, int **colIndex, double **lhs, int *conSense, double * rhs,
        void *context, 
		char *xLog, char *globalLog, 
        /*stopCriterion=None,*/
        double tol, int maxIter,
		double *x_best, double *f_best,double * x_last, double *f_last, 
		double *deltak, int * nIter, double *time);


// For debugging
void vector_show(double *x, const int n, char *title);
#endif
