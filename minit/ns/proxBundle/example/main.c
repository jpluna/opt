#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include<gurobi_c.h>
#include<proxBundle.h>
#include<proxDefaults.h>
#include<proxSolver.h>
#include<blackBoxTools.h>


int MXHILB_BB(double *x, const int n,  const int mode, double *fval, double *g, void *context)
{
	int i,j;
	double funval, aux;
	int index;
	int sign;
	funval = -1;
	index = -1;
	for(i=0; i<n; i++)
	{
		aux =0;
		for(j=0;j<n;j++)
		{
			aux += x[j]/(i + j + 1.0);
		};
		if (fabs(aux) > funval)
		{
			if(aux > 0)
			{
				sign = 1;
			}
			else
			{
				if(aux<0)
				{
					sign = -1;
				}
				else{sign = 0;};
			};
			funval = sign * aux;
			index = i;
		};
	};

	if (mode != 1)
	{
		*fval = funval;
	};
	if( mode > 0)
	{
		if(sign == 0)
		{
			for(int k=0; k<n; k++)
			{
				g[i] = 0;
			//vector_set_zero(g, n);
			};
		}
		else
		{
			if(sign>0)
			{
				for(j=0;j<n;j++)
				{
					g[j] = 1.0/(index + j + 1);
				};
			}
			else
			{
				for(j=0;j<n;j++)
				{
					g[j] = -1.0/(index + j + 1);
				};
			};
		};
	};
	return 0;
}



int main()
{
	int n=100;//problem dimension
	int maxSize=400;//bundle size
	int maxIter=200;
	int status;
	double *x0=(double *)malloc(n *sizeof(double));
	for (int i=0; i<n; i++)
	{
		x0[i] = 1;
	};

	blackBoxP bb; 
	bb= &MXHILB_BB;
	void *bbContext = NULL;

	proxLMSolver *solveProxM; 
	solveProxM = &solveProxLMDual2;
	void *solveProxMConxtext;
	solveProxMConxtext=  initProxMDual2Context();

	proxBundle *bundle; 
	bundle = initProxBundle(maxSize, n);

	stopCriterion *stopCriterionFun=NULL;

		void *stopCriterionContext = NULL;
        double m = 0.5;
		double *lb = NULL;
	   double *ub = NULL;
		int num_lcon=0;
int *conSize=NULL;
 int **colIndex=NULL;
 double **lhs=NULL;
 int *conSense=NULL;
 double * rhs=NULL;

        void *context=NULL; 
		char *xLog = "x.log";
		char *globalLog = "g.log";
        double tol= 1e-3;
		double *x_best= (double *)malloc(n * sizeof(double));
	double f_best;
	double * x_last = (double *)malloc(n * sizeof(double));
	double f_last;
 
		double deltak;
	int nIter;
	double time;
	bundleManager *bundleManagerFun = NULL; 
	void *bundleManagerContext= NULL; 

	status = proxBundleMethodMin(x0, n,  
		bb, bbContext, 
		solveProxM, solveProxMConxtext,
		bundleManagerFun,  bundleManagerContext,
		bundle,
		stopCriterionFun,
		stopCriterionContext,
        m,
		lb, ub,
		num_lcon,conSize, colIndex, lhs, conSense,  rhs,
        context, 
		xLog, globalLog, 
        tol, maxIter,
		x_best, &f_best, x_last, &f_last, 
		&deltak, & nIter, &time);
	if(status==0)
	{
		printf("nIter=%d\ndelta = %f\n f_best=%f\n time=%f\n",nIter, deltak, f_best, time);
	};
}
