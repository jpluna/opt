#include<proxBundle.h>
#include<stdlib.h>
#include<stdio.h>
#include<blackBoxTools.h>
#include<vectorOperations.h>
#include<proxSolver.h>
#include<proxDefaults.h>
#include <string.h>
#include<time.h>

void printBundle(proxBundle *bundle)
{
	int i;
	int j;
	printf("maxSize-->%d\n", bundle->maxSize);
	printf("size-->%d\n", bundle->size);
	/*for (i=0; i<bundle->maxSize;i++)*/
	for (i=0; i<bundle->size;i++)
	{
		printf("cut %d-->%d\n", bundle->cut[i][0],bundle->cut[i][1]);
			printf("sg -->");
		for (j=0; j<2;j++)
		{
			printf("%f,", bundle->c1[bundle->cut[i][1]][j]);
		};
			printf("\n");
	};
};

proxBundle * initProxBundle(int maxSize, int n)
{
	proxBundle *bundle;

	bundle = (proxBundle *)malloc(sizeof(proxBundle));
	bundle->maxSize = maxSize;
	bundle->size=0;
	bundle->absLastId=0;//Newest cut id absulute value
	//int **cut;// cut[i] = [idcut, position]. 
		bundle->cut = (int **)malloc( maxSize * sizeof(int*));
	//double **c1;// c1[position]
		bundle->c1 = (double **)malloc(maxSize * sizeof(double*));
	//double  *c0;// c0[position] 
		bundle->c0 = (double *)malloc(maxSize * sizeof(double));
	//double  *alpha;// alpha[position] mulitplier associated to the cut
		bundle->alpha = (double *)malloc(maxSize * sizeof(double));
		for(int i=0;i<maxSize; i++)
		{
			bundle->cut[i] = (int *)malloc(2 *sizeof(int));
				bundle->cut[i][0] = 0;
				bundle->cut[i][1] = i;
			bundle->c1[i] = (double *)malloc(n *sizeof(double));
		};
	return bundle;
};

void addToBundle(proxBundle *bundle, int n, int idCut, const double *sg, const double c0)
{
	int i = bundle->size;
	int absLastId =  bundle->absLastId;
	int absIdCut = idCut>=0?idCut:-idCut;
	if (bundle->size == bundle->maxSize)
	{
		i--;
		printf("warning: bundle full. Oldest cut %d will be deleted\n", bundle->cut[0][0]);
		for(int j =1;j<bundle->maxSize; j++)
		{
			bundle->cut[j-1][0]= bundle->cut[j][0];
			bundle->cut[j-1][1]= bundle->cut[j][1];
		};
	}
	else
	{
		bundle->size ++;
	};
	/*printBundle(bundle);*/



	if  (absIdCut <=  absLastId)
	{
		printf("warning:  cut id  %d is (abs)below  than (abs) lastId %d \n", absIdCut, absLastId);
		idCut = idCut>=0?absLastId + 1: -(absLastId + 1);
		printf("cut has a new id assigned %d  \n", idCut);
	};

	bundle->absLastId = idCut >= 0? idCut:-idCut;

	bundle->cut[i][0] = idCut;
	vector_mencpy(bundle->c1[bundle->cut[i][1]], sg, n);
	/*printf("end of bundle add\n");*/
	bundle->c0[bundle->cut[i][1]] = c0;
};

int stopCriterionDeltaLTTol(double *sa, const int n, const double muk, const double tol, algorithmState *state, void *stopCriterionContext)
{
	defaultStoppingContext *context;
	int stop = 0;

	if ( (*state->nIter) > 0)
	{
		if (stopCriterionContext != NULL)
		{
			context = (defaultStoppingContext *) stopCriterionContext;
			double f_best = ( *(state->f_serious) <= *(state->f_last))? (*(state->f_serious)):(*(state->f_last));

			if (context->targetValue != NULL)
			{
				if (*(context->targetValue)>= f_best) 
				{
					stop = 1;
					printf("Target value attained\n");
				};
			};

			if ((stop == 0) && (context->optimalValue != NULL)) 
			{
				if ( *(context->optimalValue) + tol>= f_best)
				{
					stop = 1;
					printf("Known optimal value attained\n");
				};
			};
		};
		if ( (stop == 0) && ((*state->delta) <= tol)) { stop=1;};
	}; 
	/*printf("iter --> %d, ", (*state->iter));*/
	return stop;
};

int maxSizeBundleManager(proxBundle *bundle, void* context)
{
	int d;
	int **cutAux;
	d = bundle->size + 2 - bundle->maxSize; // we need to make room for two extra cuts at each iteration (agregated e last trial point)

	if (d > 0) //we delete the oldest d cuts (at begining of array cut) and realocate thie proxition index at end of array cut. Note that absLastId does not change
	{
		cutAux = (int **)malloc(d * sizeof(int *));// copying positions of d inital cuts
		for (int i=0;i<d;i++)
		{
			cutAux[i] = (int *)malloc(2 * sizeof(int));
			cutAux[i][0] = bundle->cut[i][0];
			cutAux[i][1] = bundle->cut[i][1];
		};

		for (int i=0;i < bundle->size-d; i++)//moving info to the initial part
		{
			bundle->cut[i][0] = bundle->cut[i + d][0];
			bundle->cut[i][1] = bundle->cut[i + d][1];
		};

		for (int i=0;i<d;i++)//coping free positons to the end of cut array
		{
			bundle->cut[bundle->size-d+i][0] = cutAux[i][0];
			bundle->cut[bundle->size-d+i][1] = cutAux[i][1];
			free(cutAux[i]);
		};
		free(cutAux);
		bundle->size = bundle->size - d;
	};
	return 0;
};


int nonZeroMultiplierBundleManager(proxBundle *bundle, void* context)
{
	int status =0;
	double epz;
	if (context==NULL)
	{
		epz = 1e-5;
	};

	int iCutKepp = 0;// for alive cuts 
	int  iCutDel = bundle->size-1;// for dead cuts
	int **cutAux;
	int iCut;
	if (bundle->size > 0)
	{
		cutAux = (int **)malloc(bundle->size * sizeof(int *));
			for (int i=0; i<bundle->size; i++)
			{
				cutAux[i] = (int *)malloc(2 * sizeof(int));
			};

		for (int i=0; i<bundle->size; i++)// copying bundle cuts into cutAux in such a way that cutAux=[cuts that survive; cuts deleted].
		{
			iCut  = bundle->cut[i][1];
			if (bundle->alpha[iCut]<= epz) //cut should be discarded
			{
				cutAux[iCutDel][1] = iCut;
				iCutDel --;
			}
			else//cut will be kept
			{
				cutAux[iCutKepp][0] = bundle->cut[i][0];//id cut
				cutAux[iCutKepp][1] = iCut;//cut index in the bundle
				iCutKepp ++;
			};
		};
		for (int i=0; i<bundle->size; i++)
		{
			bundle->cut[i][0] = cutAux[i][0];//id
			bundle->cut[i][1] = cutAux[i][1];//bundle index
			free(cutAux[i]);
		};
		free(cutAux); 
		bundle->size = iCutKepp;
		if (iCutKepp >0)
		{
			int idAux = bundle->cut[iCutKepp -1][0];
			bundle->absLastId = idAux>=0?idAux: -idAux;
		}
		else
		{
			bundle->absLastId=0;
		};

		status =  maxSizeBundleManager(bundle, context);
	};

	return status;
};


double proxParamDefault(int n, double *xk, double *sk, double *zkprox, double *skprox, int iterK, int changeProxCenter, void * context)
{
	//The context is a 2D array. context[0]--> seriousStepMu, context[1]--> nullStepMu
	double newMu;
	double prevMu;
	double *dx, *ds;
	double dsds, extra, aux;
	double *mu = (double *) context;
	
	if (changeProxCenter == 1)
	{
		prevMu = mu[0];
		dx = (double *)malloc(n * sizeof (double));
		ds = (double *)malloc(n * sizeof (double));
        //dx = zkprox - xk
			vector_mencpy(dx, zkprox, n);
			vector_sub(dx, xk, n);
        //ds = skprox - sk
			vector_mencpy(ds, skprox, n);
			vector_sub(ds, sk, n);
        dsds = vector_inner_prod(ds,ds, n);
        if (dsds < 1e-5)
		{
            extra = 0;
		} else
		{
            extra = vector_inner_prod(dx, ds, n)/dsds;
		/*printf("extra=%f\n",extra);*/
		};

        aux = (1.0/prevMu) + extra;
        newMu = 1.0/aux;
		mu[0]= newMu;
		mu[1] = newMu;
		free(dx);
		free(ds);
		/*printf("Smu=%f\n",newMu);*/
	} else
	{
		prevMu = mu[1];
        newMu = prevMu + 0.1;
		mu[1] = newMu;
	};
    return newMu;
};

int proxBundleMethod(double *x0, int n,  
		blackBoxP bb, void *bbContext, 
		proxLMSolver *solveProxM, void *solveProxMConxtext,
		proxParamManager *proxParamManagerFun, void * proxParamContext,
		bundleManager *bundleManagement, void * bundleManagementContext,
		proxBundle *bundle,
        double m,
		double *lb, double *ub,
		int num_lcon,int *conSize, int **colIndex, double **lhs, int *conSense, double * rhs,
        void *context, 
		char *xLog, char *globalLog, 
		stopCriterion *stopCriterionFun, void *stopCriterionContext,
        double tol, int maxIter,
		double *x_best, double *f_best,double * x_last, double *f_last, 
		double *deltak, int * nIter, double *time)
{
	clock_t startTime = clock();
	int status=0;
	int optimizationOK=0;
	int j;
	int bbMode, bbStatus;
	double *fk, *fzkprox;
	double c0Aux; 
	double *xk, *zkprox;
	double *vectorAux;
	double *skprox,*sk;
	double mu, muk;
    int *iterK, seriousK;
	double c0a;
    int newBundleId;
	int bundleSize;
	int changeProxCenter=0;
	double proxOptVal;
	char nullLog[10];

	FILE *gLogPtr;
	FILE *xLogPtr;

	algorithmState *state;

	//printf("JP OK0\n");
	state = (algorithmState *)malloc(sizeof(algorithmState));//It is very important to note that the state structure just shares memory with the variablesin the arguments (e.g. x_best, x_last, delta, etc. But x_serious is not)
		state->n = n;
		state->f_serious = f_best;
			fk = f_best;
		state->x_serious = x_best;
			xk = x_best;

		state->x_last = x_last;
			zkprox = x_last;
		state->f_last = f_last;
			fzkprox = f_last;
		state->delta = deltak;

		state->nIter = nIter;
			iterK = nIter;
	/*vector_show(x0, n, "x0");*/

	if (xLog != NULL)
	{
		printf("xlog to %s\n", xLog);
		xLogPtr = fopen(xLog, "w");
	};

	if (globalLog != NULL) 
	{ 
		printf("global log to %s\n", globalLog);
		gLogPtr = fopen(globalLog, "w");
	};
    if (context == NULL)
	{
		printf("proxBundleMethod: context is null\n");
	};

	//printf("JP OK1\n");
	vectorAux = (double *)malloc(n * sizeof(double));
	/*xk = (double *)malloc(n * sizeof(double));*/
	/*xk = x_best;*/
	/*state->x_serious = xk;*/
	vector_mencpy(xk, x0, n);
	/*vector_show(xk, n, "x0afterC");*/

	/*zkprox = (double *)malloc(n * sizeof(double));*/
	/*zkprox = x_last;*/

	//printf("JP OK2\n");
	sk = (double *)malloc(n * sizeof(double));
	double *sa = (double *)malloc(n * sizeof(double));
	skprox = (double *)malloc(n * sizeof(double));
	/*vector_mencpy(xk, x0, n);// prox center*/

	bbMode = 2;
	/*vector_show(xk, n, "x0before BB");*/
	bbStatus =  (*bb)(xk, n, bbMode, fk, sk, bbContext);
	//printf("JP OK2.1\n");
	/*vector_show(xk, n, "xkafter BB");*/
	c0Aux = *fk - vector_inner_prod(xk, sk, n);
	/*vector_show(xk, n, "xkbeforeAddBundle BB");*/

	//printf("JP OK2.2 Before adding cut to the bundle\n");
    newBundleId = bundle->absLastId + 1; //regular cut
	addToBundle(bundle, n, newBundleId, sk, c0Aux); // cut(x) = fk - sk*xk + sk*x
	//printf("JP OK2.3 After adding cut to the bundle\n");
	/*printf("after bundle add \n");*/
    *iterK = 0;
	/*vector_show(xk, n, "xkafterAddBundle BB");*/



	//printf("JP OK3\n");
	/*vector_show(xk, n, "xkbefore proxParam");*/
	muk = proxParamManagerFun(n, xk, sk, zkprox, skprox, *iterK, changeProxCenter, proxParamContext);
    mu = muk;
	/*vector_show(xk, n, "xkafter proxParam");*/
	//printf("JP OK4\n");

    seriousK = 0;
	strcpy(nullLog, "");
    (*deltak)= tol  + 1;// # ensuring the first iteration

	//printf("JP OK9\n");
	if (xLog != NULL)
	{
		fprintf(xLogPtr, "[k=%d][serious=%d %s]", *iterK, seriousK, nullLog);
		for (j=0;j < n; j++)
		{
			fprintf(xLogPtr, ",%f", xk[j]);
		};
		fprintf(xLogPtr, "\n");
				
	};


	if (globalLog != NULL) 
	{
		fprintf(gLogPtr, "[k=%d],[serious=%d %s],f(xk)=%f,f(z k+1)=%f,deltak=%f,proxOptVal=%f, mu=%f,bundleSize=%d\n", *iterK, seriousK, nullLog,*fk, 0.0, (*deltak), 0.0, 0.0, 0);
	};
    
	//printf("JP OK10\n");

	/*vector_show(xk, n, "xk0");*/
    while ((*iterK < maxIter) && (optimizationOK ==0 ) && (status ==0))
	{
	/*printBundle(bundle);*/
        *iterK +=1;
		bundleSize = bundle->size;
        /*if bundleManagementContext is not None:*/
            /*if 'cutId' in bundleManagementContext:*/
                /*bundleManagementContext['cutId'] = newBundleId*/
		status = (*solveProxM)(xk, n, fk, bundle, lb, ub, num_lcon, conSize, colIndex, lhs, conSense, rhs, &muk, bundleManagement, bundleManagementContext, sa, zkprox, &proxOptVal, deltak, &c0a, solveProxMConxtext);
				
		maxSizeBundleManager(bundle, NULL);
		/*deltak = fk - proxOptVal;*/
		/*ea = deltak - (vector_inner_prod(sa, sa, n)/(2 * muk));*/

		newBundleId = -(bundle->absLastId + 1); //agregated cut
		addToBundle(bundle, n, newBundleId, sa, c0a); //cut(x) = fk + sa*x -sa*xk - ea

		bbMode=2;
		bbStatus =  (*bb)(zkprox, n, bbMode, fzkprox, skprox, bbContext);
		/*printf("%f\n", fzkprox);*/

		//adding new cut
		c0Aux = (*fzkprox) - vector_inner_prod(skprox,zkprox, n);
		newBundleId = bundle->absLastId + 1; //regular cut
		addToBundle(bundle, n, newBundleId, skprox, c0Aux);

        /*# Checking if a serious step was reached*/
        if  ( (*fzkprox) < *fk - m * (*deltak))
		{// # we have a serious step
			strcpy(nullLog,"");
            changeProxCenter = 1;

            /*# Updating the prox center */
			vector_mencpy(xk, zkprox, n);
            *fk = *fzkprox;
            
            seriousK = *iterK;
            mu = muk;
		}
        else
		{//: # null step
			strcpy(nullLog, "-null");
            changeProxCenter = 0;
		};

		muk = proxParamManagerFun(n, xk, sk, zkprox, skprox, *iterK, changeProxCenter, proxParamContext);

		if (xLog != NULL)
		{
			fprintf(xLogPtr, "[k=%d][serious=%d %s]", *iterK, seriousK, nullLog);
			for (j=0;j < n; j++)
			{
				fprintf(xLogPtr, ",%f", zkprox[j]);
			};
			fprintf(xLogPtr, "\n");
					
		};
		if (globalLog != NULL) 
		{
			fprintf(gLogPtr, "[k=%d],[serious=%d %s],f(xk)=%f,f(z k+1)=%f,deltak=%f,proxOptVal=%f, mu=%f,bundleSize=%d\n", *iterK, seriousK, nullLog,*fk, *fzkprox, (*deltak), proxOptVal, muk, bundleSize);
		};

	optimizationOK = stopCriterionFun(sa, n,  muk, tol, state, stopCriterionContext);
	};

	if (xLog != NULL) { fclose(xLogPtr);};
	if (globalLog != NULL) { fclose(gLogPtr);};
	if (*fzkprox<*fk)
	{
		*fk = *fzkprox;
		vector_mencpy(x_best, zkprox, n);
	};
	 /**f_best=fk;*/
	 /*vector_mencpy(x_last, zkprox, n);*/
	 /**f_last=fzkprox;*/
	 /**nIter=*iterK;*/
	clock_t endTime = clock();
	*time = (double) (endTime - startTime)/CLOCKS_PER_SEC;
	if (optimizationOK)
	{
		status =0;
	}
	else
	{
		if (status == 0)
		{
			if (*(state->nIter)>= maxIter)
			{
			printf("Maximum number of iterations (maxIter= %d) reached without satisfying optimal conditions\n", maxIter);
			status = 10;
			};
		};
	};
	return status;
};


int proxBundleMethodMin(double *x0, int n,  
		blackBoxP bb, void *bbContext, 
		proxLMSolver *solveProxM, void *solveProxMConxtext,
		bundleManager *bundleManagerFun, void * bundleManagerContext,
		proxBundle *bundle,
		stopCriterion *stopCriterionFun,
		void *stopCriterionContext,
        double m,
		double *lb, double *ub,
		int num_lcon,int *conSize, int **colIndex, double **lhs, int *conSense, double * rhs,
        void *context, 
		char *xLog, char *globalLog, 
        double tol, int maxIter, 
		double *x_best, double *f_best,double * x_last, double *f_last, 
		double *deltak, int * nIter, double *time)
{ 
	int status;

	proxParamManager * proxParamManagerFun = NULL;
	void *proxParamContext= NULL; 

	if (solveProxM == NULL)
	{
		solveProxM = &solveProxLMDual;
		printf("proxBundle.c: primal solveProxLMDual  selected\n");
		/*solveProxM = &solveProxLMPrimal;*/
		/*printf("proxBundle.c: primal proxLMsolver  selected\n");*/
	};

	if (bundleManagerFun == NULL)
	{
		bundleManagerFun = &maxSizeBundleManager;
	};

	if (proxParamManagerFun == NULL)
	{
		proxParamManagerFun = &proxParamDefault;
		if (proxParamContext == NULL)
		{
			double *auxMu = (double *)malloc(2 * sizeof(double));
			auxMu[0] = 0.5;
			auxMu[1] = 0.5;
			proxParamContext = (void *) auxMu;
		};
	};
	if (stopCriterionFun == NULL)
	{
		stopCriterionFun = &stopCriterionDeltaLTTol;
	};
	status = proxBundleMethod(x0, n,  bb, bbContext, 
				solveProxM, solveProxMConxtext, 
				proxParamManagerFun, proxParamContext,
				bundleManagerFun,  bundleManagerContext,
				bundle,  m, 
				lb, ub, 
				num_lcon, conSize, colIndex, lhs, conSense, rhs,
				context, 
				xLog, globalLog, 
				stopCriterionFun,stopCriterionContext, 
				tol, maxIter, x_best, f_best, x_last, f_last, 
				deltak,  nIter, time); 
	return status;
};



// For debugging
void vector_show(double *x, const int n, char *title)
{
	printf("\n %s = ", title);
	for(int i=0; i<n; i++)
	{
		printf("%f, ", x[i]);
	};
	printf("\n");
};
