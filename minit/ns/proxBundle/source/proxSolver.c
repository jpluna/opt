#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include<blackBoxTools.h>
#include<vectorOperations.h>
#include<proxSolver.h>
#include<proxBundle.h>
#include<gurobi_c.h>


int solveProxLMDual(double *xk, const int n, double *fk, proxBundle *bundle, double *lb, double *ub,
		int num_lcon,int *conSize, int **colIndex, double **lhs, int *conSense, double * rhs,
		double *muk, bundleManager * bundleManagerFun, void * bundleManagerContext, 
		double *sa,  double *zkprox, double *proxOptVal,
		double *deltak, double *c0a, void *context)
{
	/*printf("dual- dual- dual- dual- dual- dual- dual- dual- dual\n");*/

	int i,j;
	int status = 0;
    int nb = bundle->size;
	double *vectorAux = (double *)malloc(n * sizeof(double));
	vector_set_zero(sa, n);
	double optVal;
	double valAux;
	double epz=1e-6;

	if ((lb != NULL) || (ub !=NULL))
	{
		printf("Warning solveProxLMDual: lb or ub non NULL. This method is dicarding those bounds\n");
	};

	/*gurobi*/
	int numvars= nb;
	double *alpha = (double *)malloc(numvars * sizeof(double));
	double *alphaLb = (double *)malloc(numvars * sizeof(double));
	vector_set_zero(alphaLb, numvars);
	double *alphaUb = (double *)malloc(numvars * sizeof(double));
	vector_set_all(alphaUb, 1.0, numvars);
	int error = 0;

        /*# Setting the objective function*/
        /*objF = grb.quicksum(b[-1] * alpha[i] for i, (id_b, b) in enumerate(bundle))*/
        /*objF += grb.quicksum(np.dot(bundle[i][1][0], bundle[j][1][0]) * alpha[i] * alpha[j] for i in bIndex for j in bIndex) / (2 * muk)*/
        /*m.setObjective(objF, sense=grb.GRB.MINIMIZE)*/
	// setting the quadratic part
	int *qrow = (int *)malloc(numvars * numvars * sizeof(int));
	int *qcol = (int *)malloc(numvars * numvars * sizeof(int));
	double *qval = (double *)malloc(numvars * numvars * sizeof(double));
	int numqnz=0;
	for (i=0;i<numvars;i++)
	{
		for (j=0;j<i;j++)
		{
			valAux = vector_inner_prod(bundle->c1[bundle->cut[i][1]], bundle->c1[bundle->cut[j][1]],n); 
			if (fabs(valAux)> epz)
			{
				qrow[numqnz] = i;
				qcol[numqnz] = j;
				qval[numqnz] =  valAux/(*muk); // 2*valAux/(2*muk)
				numqnz ++;
			};
		};
		valAux = vector_inner_prod(bundle->c1[bundle->cut[i][1]], bundle->c1[bundle->cut[i][1]],n); 
		qrow[numqnz] = i;
		qcol[numqnz] = i;
		qval[numqnz] = valAux/(2 * (*muk));
		numqnz ++;
	};


	// setting the linear part
		double *linObj = (double *)malloc(numvars * sizeof(double));
		for (i=0;i<numvars; i++)
		{
			linObj[i] = -vector_inner_prod(xk, bundle->c1[bundle->cut[i][1]], n) -   bundle->c0[bundle->cut[i][1]];
		};

		GRBenv *env = NULL;
		GRBmodel *model = NULL;
		/* Create environment */
		/*error = GRBemptyenv(&env);*///I do not know why this does not work
		error = GRBloadenv(&env,"dual.log");
		/*if (error) goto QUIT;*/
		/* Create an empty model */
		/*error = GRBnewmodel(env, &model, "dual solver", numvars, linObj, lb, ub, NULL, NULL);*/
		error = GRBnewmodel(env, &model, "dual solver", numvars, linObj, alphaLb, alphaUb, NULL, NULL);
		/*if (error) goto QUIT;*/
		error = GRBaddqpterms(model, numqnz, qrow, qcol, qval);
		for (i=0;i<numvars; i++)
		{
			error = GRBsetdblattrelement(model, GRB_DBL_ATTR_OBJ, i, linObj[i]);
		};

		error = GRBsetintparam(GRBgetenv(model), "LogToConsole", 0);

		/*# Constrain sum alpha ==1*/
		/*m.addConstr(grb.quicksum(alpha) == 1, name='sum1')*/
		int *ind=(int *)malloc(numvars * sizeof(int));
		double *constrSum1Val=(double *)malloc(numvars * sizeof(double));
		double *obj=(double *)malloc(numvars * sizeof(double));

		for(i=0;i<numvars;i++)
		{
			ind[i] = i;
			constrSum1Val[i] = 1.0;
		};

		error = GRBaddconstr(model, numvars, ind, constrSum1Val, GRB_EQUAL, 1.0, "sum==1");


		/*# Solving the problem*/
		/*# m.write('jj.lp')*/
		/*# m.write('jj.mps')*/
		/*error = GRBwrite(model, "dual.lp");*/
		/*m.optimize()*/
		/* Solve */
		error = GRBoptimize(model);
		/*alphak = np.zeros(nb)*/
		/*optVal = m.objVal*/
		error = GRBgetintattr(model, GRB_INT_ATTR_STATUS, &status);
		if (status != GRB_OPTIMAL)
		{
			printf("warning: gurobi did not solve the problem. Gurobi status:%d\n", status);
			status=1;
		}
		else
		{
			status = 0;
		};
		error = GRBgetdblattr(model, GRB_DBL_ATTR_OBJVAL, &optVal);
		error = GRBgetdblattrarray(model, GRB_DBL_ATTR_X, 0, numvars, alpha);


		/*printf("(%d), alpha-->", numvars);*/
		/*for(i=0;i<numvars; i++)*/
		/*{*/
		/*printf("%f,", alpha[i]);*/

		/*};*/
		/*printf("\n");*/

		/*for i, (id_b, b) in enumerate(bundle):*/
		/*sa += alpha[i].x * b[0]*/
		/*ea += alpha[i].x * b[-1]*/
		/*# alphak[i] = alpha[i].x*/
		*c0a = 0;
		for(i=0;i<numvars;i++)
		{
			vector_mencpy(vectorAux, bundle->c1[bundle->cut[i][1]], n);
			vector_scale(vectorAux, alpha[i], n);
			vector_add(sa, vectorAux, n);

			*c0a += alpha[i]*bundle->c0[bundle->cut[i][1]];
		};

		/*[>alphak = {id_b: alpha[i].x for i, (id_b, b) in enumerate(bundle)}<]*/
		/**ea = (*fk) - vector_inner_prod(sa, xk, n) - (*ea);*/


		/*if (*ea<0)*/
		/*{*/
			/*printf("proximal model solver: aggregated error is negative. It will set to zero\n");*/
			/**ea = 0.0;*/
		/*};*/

    /*zkprox = xk - (sa/ muk)*/
		vector_mencpy(vectorAux, sa, n);
		vector_scale(vectorAux, (1/(*muk)), n);
		vector_mencpy(zkprox, xk, n);
		vector_sub(zkprox, vectorAux, n);
    /*proxOptVal =  - optVal*/
		*proxOptVal =   - optVal;
		*deltak = (*fk) - (*proxOptVal);
    /*stat = 0*/
    /*# compressing the bundle*/
    /*bundleManagementContext.update({'alpha': alphak})*/
		if (bundleManagerFun != NULL)
		{
			bundleManagerFun(bundle, bundleManagerContext);
		};
    /*return sa, ea, zkprox, proxOptVal, deltak, bundle, stat*/
	 /* Free model */
  GRBfreemodel(model);

  /* Free environment */
  GRBfreeenv(env);
  free(vectorAux);
  free(alpha);
  free(alphaLb);
  free(alphaUb);
  free(linObj);
  free(ind);
  free(obj);
  free(constrSum1Val);
  return status;
};

int solveProxLMPrimal(double *xk, const int n, double *fk, proxBundle *bundle, double *lb, double *ub,
		int num_lcon,int *conSize, int **colIndex, double **lhs, int *conSense, double * rhs,
		double *muk, bundleManager * bundleManagerFun, void * bundleManagerContext, 
		double *sa,  double *zkprox, double *proxOptVal,
		double *deltak, double *c0a, void *context)
{
	/*printf("primal- primal- primal- primal- primal- primal- primal- primal- primal\n");*/
	double epz = 1e-6;

	int i,j;
	int status = 0;
	int nb = bundle->size;
	double *vectorAux = (double *)malloc(n * sizeof(double));
	vector_set_zero(sa, n);
	/**ea =0;*/
	double optVal;
	char strAux[20];

	/*[>gurobi<]*/
	int numvars= n + 1;
	/*[>double *alpha = (double *)malloc(numvars * sizeof(double));<]*/

	double *xtLb = (double *)malloc( numvars * sizeof(double));
	if (lb == NULL)
	{
		vector_set_all(xtLb, -GRB_INFINITY , numvars);
	}
	else
	{
		vector_mencpy(xtLb, lb, n);
		xtLb[n] = -GRB_INFINITY;
	};

	double *xtUb = (double *)malloc( numvars * sizeof(double));
	if (ub == NULL)
	{
		vector_set_all(xtUb, GRB_INFINITY , numvars);
	}
	else
	{
		vector_mencpy(xtUb, ub, n);
		xtUb[n] = GRB_INFINITY;
	};
	int error = 0;

		/*[># Setting the objective function<]*/
		/*[>objF = grb.quicksum(b[-1] * alpha[i] for i, (id_b, b) in enumerate(bundle))<]*/
		/*[>objF += grb.quicksum(np.dot(bundle[i][1][0], bundle[j][1][0]) * alpha[i] * alpha[j] for i in bIndex for j in bIndex) / (2 * muk)<]*/
		/*[>m.setObjective(objF, sense=grb.GRB.MINIMIZE)<]*/
	// setting the quadratic part
	int *qrow = (int *)malloc(n * sizeof(int));
	int *qcol = (int *)malloc( n * sizeof(int));
	double *qval = (double *)malloc( n * sizeof(double));
	int numqnz=n;
	for (i=0;i<numqnz;i++)
	{
		qrow[i] = i;
		qcol[i] = i;
		qval[i] =  (*muk)/2.0;
	};

	// setting the linear part
		double *linObj = (double *)malloc( numvars * sizeof(double));
		vector_mencpy(linObj, xk, n);
		vector_scale(linObj, -(*muk), n);
		linObj[n] = 1;

		GRBenv *env = NULL;
		GRBmodel *model = NULL;
		/*[> Create environment <]*/
		/*[>error = GRBemptyenv(&env);<]//I do not know why this does not work*/
		error = GRBloadenv(&env,"primal.log");
		/*[>if (error) goto QUIT;<]*/
		/*[> Create an empty model <]*/
		/*[>error = GRBnewmodel(env, &model, "dual solver", numvars, linObj, lb, ub, NULL, NULL);<]*/
		error = GRBnewmodel(env, &model, "primal solver", numvars, linObj, xtLb, xtUb, NULL, NULL);
		/*[>if (error) goto QUIT;<]*/
		error = GRBaddqpterms (model, numqnz, qrow, qcol, qval);
		for (i=0;i<numvars; i++)
		{
			error = GRBsetdblattrelement(model, GRB_DBL_ATTR_OBJ, i, linObj[i]);
		};

		error = GRBsetintparam(GRBgetenv(model), "LogToConsole", 0);

		error = GRBupdatemodel(model);
		if (error != 0) {printf("error: gurobi failed to update model after setting obj function \n");};
		
		int *ind=(int *)malloc(numvars * sizeof(int));
		double *cutConstr=(double *)malloc(numvars * sizeof(double));

		for(i=0;i<numvars;i++)
		{
			ind[i] = i;
		};
		cutConstr[numvars - 1] = -1;
		for (i=0;i<nb;i++)
		{
			j = bundle->cut[i][1];
			vector_mencpy(cutConstr, bundle->c1[j], n);
			sprintf(strAux, "cut [%d]", bundle->cut[j][0]);
			error = GRBaddconstr(model, numvars, ind, cutConstr, GRB_LESS_EQUAL,  -bundle->c0[j],strAux);
			if (error != 0)
			{
				printf("error: gurobi failed to add a constraint cut\n");
			};
		};
		error = GRBupdatemodel(model);
		if (error != 0)
		{
			printf("error: gurobi failed to update model after adding cuts\n");
		};

		//adding model linear constraints
		/*printf("addin linear constraints: %d\n", num_lcon);*/
		for(i=0;i<num_lcon; i++)
		{
			/*printf("constraints [%d]: size %d\n", i, conSize[i]);*/
			/*printf("constraints [%d]: sense %d\n", i, conSense[i]);*/
			/*printf("constraints [%d]: rhs %f\n", i, rhs[i]);*/
			

			if (conSense[i] < 0)
			{ 
				error = GRBaddconstr(model, conSize[i], colIndex[i], lhs[i],GRB_LESS_EQUAL  ,rhs[i], "linear_con");
			}
			else
			{ 
				if (conSense[i] > 0)
				{ 
					error = GRBaddconstr(model, conSize[i], colIndex[i], lhs[i],GRB_GREATER_EQUAL  ,rhs[i], "linear_con");
				}
				else
				{
					error = GRBaddconstr(model, conSize[i], colIndex[i], lhs[i], GRB_EQUAL ,rhs[i], "linear_con");
				};
			};

			if (error != 0)
			{
				printf("error: gurobi failed to add a linear constraint \n");
				printf("size %d\n", conSize[i]);
				printf("sense %d\n", conSense[i]);
				printf("rhs %f\n", rhs[i]);
			};

		};
		/*printf(" linear constraints added in gurobi\n ");*/


		/*[># Solving the problem<]*/
		/*[># m.write('jj.mps')<]*/
		error = GRBwrite(model, "primal.lp");

		/*[> Solve <]*/
		error = GRBoptimize(model);
		if (error != 0) {printf("error: gurobi raized error code when optimizing model\n");};
		/*[>alphak = np.zeros(nb)<]*/
		/*[>optVal = m.objVal<]*/
		error = GRBgetintattr(model, GRB_INT_ATTR_STATUS, &status);

		if (status != GRB_OPTIMAL)
		{
			printf("error: gurobi did not solve the problem. Gurobi status:%d\n", status);
			status = 1;
		}
		else
		{
			status=0;
			error = GRBgetdblattr(model, GRB_DBL_ATTR_OBJVAL, &optVal);
			if (error != 0) {printf("error: gurobi  could not get optimizal value from model\n");};
			
			error = GRBgetdblattrarray(model, GRB_DBL_ATTR_X, 0, numvars - 1 , zkprox);



			/*sa =  muk * (xk - zkprox)*/
			vector_mencpy(sa, xk, n);
			vector_sub(sa, zkprox, n);
			vector_scale(sa, *muk, n);
			*proxOptVal = optVal + ((*muk) * vector_inner_prod(xk, xk, n) /2.0);
			*deltak = (*fk) - (*proxOptVal);
			if (*deltak<0) {printf("warning: deltak = %f \n", *deltak);};
			double ea = (*deltak) - (vector_inner_prod(sa, sa, n)/ (2 * (*muk)));
			*c0a = (*fk) - vector_inner_prod(sa, xk, n) - ea;


		/*[># compressing the bundle<]*/
		/*[>bundleManagementContext.update({'alpha': alphak})<]*/
			if (bundleManagerFun != NULL)
			{
				bundleManagerFun(bundle, bundleManagerContext);
			};
		};
	/*[>return sa, ea, zkprox, proxOptVal, deltak, bundle, stat<]*/
	 /*[> Free model <]*/
  GRBfreemodel(model);

  /*[> Free environment <]*/
  GRBfreeenv(env);
  free(vectorAux);
  /*free(alpha);*/
  free(xtLb);
  free(xtUb);
  free(linObj);
  free(ind);
  /*[>free(obj);<]*/
  /*free(constrSum1Val);*/
  return status;
};

void *  initProxMDual2Context()
{
	gurobiModelContext *gContext = (gurobiModelContext *)malloc(sizeof(gurobiModelContext));
	gContext->env = NULL;
	gContext->model = NULL;
	return (void *)gContext;
};

int solveProxLMDual2(double *xk, const int n, double *fk, proxBundle *bundle, double *lb, double *ub,
		int num_lcon,int *conSize, int **colIndex, double **lhs, int *conSense, double * rhs,
		double *muk, bundleManager * bundleManagerFun, void * bundleManagerContext, 
		double *sa,  double *zkprox, double *proxOptVal,
		double *deltak, double *c0a, void *context)
{
	/*printf("C:dual 2\n");*///JP
	/*printf("dual- dual- dual- dual- dual- dual- dual- dual- dual\n");*/

	int i,j;
	int status = 0;
    int nb = bundle->size;
	double *vectorAux = (double *)malloc(n * sizeof(double));
	vector_set_zero(sa, n);
	double optVal;
	double valAux;
	double epz=1e-6;
	GRBenv *env;
	GRBmodel *model;
	int error = 0;

	gurobiModelContext *gContext;
	int numvars= bundle->maxSize;//we allocate a variable each entry in the maximum size bundle

	if ((lb != NULL) || (ub !=NULL))
	{
		printf("Warning solveProxLMDual: lb or ub non NULL. This method is dicarding those bounds\n");
	};

	
	if (context == NULL)
	{
		printf("Warning solveProxLMDual2: context has NULL value. solveProxLMDual routine will be used\n");
		status= solveProxLMDual(xk, n, fk, bundle, lb, ub, num_lcon, conSize, colIndex, lhs, conSense, rhs, muk, bundleManagerFun, bundleManagerContext, sa, zkprox, proxOptVal, deltak, c0a, context);

	}
	else
	{
		/*printf("solveProxLMDual2: nonNULL context\n");*///JP
		gContext = (gurobiModelContext *) context;
		if (gContext->model == NULL)//gurobi model was not yet built
		{
			/* Create environment */
			/*error = GRBemptyenv(&env);*///I do not know why this does not work
			/*GRBenv *env; */
			/*GRBmodel *model; */
			/*gurobi*/

			double *alphaLb = (double *)malloc(numvars * sizeof(double));
			vector_set_zero(alphaLb, numvars);
			double *alphaUb = (double *)malloc(numvars * sizeof(double));
			vector_set_all(alphaUb, 1.0, numvars);

			char *vType = (char *)malloc(numvars * sizeof(char));
			for (int i=0; i<numvars; i++)
			{
				vType[i] = GRB_CONTINUOUS;
			};

			error = GRBloadenv(&(gContext->env),"dual2.log");
			env = gContext->env;

			/*if (error) goto QUIT;*/
			/* Create an empty model */
			/*error = GRBnewmodel(env, &model, "dual solver", numvars, linObj, lb, ub, NULL, NULL);*/
			error = GRBnewmodel(env, &(gContext->model), "dual solver 2", numvars, NULL, alphaLb, alphaUb, vType, NULL);
			model = gContext->model;

			//adding the unique constraint
			/*# Constrain sum alpha ==1*/
			/*m.addConstr(grb.quicksum(alpha) == 1, name='sum1')*/
			int *ind=(int *)malloc(numvars * sizeof(int));
			double *constrSum1Val=(double *)malloc(numvars * sizeof(double));

			for(int i=0;i<numvars;i++)
			{
				ind[i] = i;
				constrSum1Val[i] = 1.0;
			};

			error = GRBaddconstr(model, numvars, ind, constrSum1Val, GRB_EQUAL, 1.0, "sum==1");

			error = GRBupdatemodel(model); 

			free(alphaLb); 
			free(alphaUb);
			free(vType);
			free(constrSum1Val); 
			free(ind);
		};
		

		env = gContext->env;
		model = gContext->model;

		int iCut;
		int jCut;
		//open values of variables associated to existing cuts
		for(int i=0; i<bundle->size; i++)
		{ 
			iCut = bundle->cut[i][1];
			error = GRBsetdblattrelement(model, GRB_DBL_ATTR_UB, iCut, 1.0);
		};

		//Fixing values of variables associated to nonexisting cuts
		for(int i=bundle->size; i<bundle->maxSize; i++)
		{ 
			iCut = bundle->cut[i][1];
			error = GRBsetdblattrelement(model, GRB_DBL_ATTR_UB, iCut, 0.0);
		};

		// setting the quadratic part
		int *qrow = (int *)malloc(nb * nb * sizeof(int));
		int *qcol = (int *)malloc(nb * nb * sizeof(int));
		double *qval = (double *)malloc(nb * nb * sizeof(double));
		int numqnz=0;
		for (int i=0;i<nb;i++)
		{
			iCut = bundle->cut[i][1];
			for (int j=0;j<i;j++)
			{
				jCut = bundle->cut[j][1];
				valAux = vector_inner_prod(bundle->c1[iCut], bundle->c1[jCut],n); 
				if (fabs(valAux)> epz)
				{
					qrow[numqnz] = iCut;
					qcol[numqnz] = jCut;
					qval[numqnz] =  valAux/(*muk); // 2*valAux/(2*muk)
					numqnz ++;
				};
			};
			valAux = vector_inner_prod(bundle->c1[iCut], bundle->c1[iCut],n); 
			qrow[numqnz] = iCut;
			qcol[numqnz] = iCut;
			qval[numqnz] = valAux/(2 * (*muk));
			numqnz ++;
		};

		// setting the linear part
		double *linObj = (double *)malloc(numvars * sizeof(double));
		vector_set_zero(linObj, numvars);
		for (int i=0;i<nb; i++)
		{
			iCut = bundle->cut[i][1];
			linObj[iCut] = -vector_inner_prod(xk, bundle->c1[iCut], n) -   bundle->c0[iCut];
		};

		//adding qudratic otem 
		error = GRBdelq(model);
		error = GRBaddqpterms (model, numqnz, qrow, qcol, qval);

		//adding the linear part
		for (int i=0;i<numvars; i++)
		{
			error = GRBsetdblattrelement(model, GRB_DBL_ATTR_OBJ, i, linObj[i]);
		};

		error = GRBsetintparam(GRBgetenv(model), "LogToConsole", 0);

		/*error = GRBwrite(model, "dual2.lp");*///JP


		/* Solve */
		error = GRBoptimize(model);
		/*alphak = np.zeros(nb)*/
		/*optVal = m.objVal*/
		error = GRBgetintattr(model, GRB_INT_ATTR_STATUS, &status);
		if (status != GRB_OPTIMAL)
		{
			printf("warning: gurobi did not solve the problem. Gurobi status:%d\n", status);
			status=1;
		}
		else
		{
			status = 0;
		};
		error = GRBgetdblattr(model, GRB_DBL_ATTR_OBJVAL, &optVal);

		double *alpha = (double *)malloc(numvars * sizeof(double));
		error = GRBgetdblattrarray(model, GRB_DBL_ATTR_X, 0, numvars, alpha);

		*c0a = 0;
		for(int i=0;i<nb;i++)
		{
			iCut = bundle->cut[i][1];
			vector_mencpy(vectorAux, bundle->c1[iCut], n);
			vector_scale(vectorAux, alpha[iCut], n);
			vector_add(sa, vectorAux, n);

			*c0a += alpha[iCut] * bundle->c0[iCut];

			bundle->alpha[iCut] = alpha[iCut];
		};


		/*zkprox = xk - (sa/ muk)*/
			vector_mencpy(vectorAux, sa, n);
			vector_scale(vectorAux, (1/(*muk)), n);
			vector_mencpy(zkprox, xk, n);
			vector_sub(zkprox, vectorAux, n);
		/*proxOptVal =  - optVal*/
			*proxOptVal =   - optVal;
			*deltak = (*fk) - (*proxOptVal);
		/*stat = 0*/
		/*# compressing the bundle*/
		/*bundleManagementContext.update({'alpha': alphak})*/
			if (bundleManagerFun != NULL)
			{
				bundleManagerFun(bundle, bundleManagerContext);
			};
		/*return sa, ea, zkprox, proxOptVal, deltak, bundle, stat*/
		 /* Free model */
	  /*GRBfreemodel(model);*/

	  /* Free environment */
	  /*GRBfreeenv(env);*/
			
	  free(vectorAux);
	  free(alpha);
	  free(linObj);
	  /*free(obj);*/
	};

  return status;
};
