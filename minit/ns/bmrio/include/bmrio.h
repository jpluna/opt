#ifndef BMRIO_H
#define BMRIO_H

#include<stdlib.h>
#include<math.h>

typedef void (*bmrioBBp)(double *xk, int n, double *fk, double *gk, int *indic, void *context );

/*
 * input:
 * ======
 * xk: current point
 * n: dimension
 * context: method context
 *
 * output:
 * =======
 * fk, gk: function value and subgradient
 * indic: <0 stop by error, =0 stop by user request. Otherwise its value should not be modifed by the subroutine
 */

int BMRIOmin(double *x0, int n, bmrioBBp oracle_fn, void * BBContext,  int memax, int imp, int impFile, int mt, double errabs, double *lb, double *ub, double *x_last, double *f_last, double *x_serious, double *f_serious, double *time, int maxBBCall, double relTol);
/*
 * input:
 * ======
 * xk: current point
 * n: dimension
 * fk: value at xk
 * bundle: 
 * lb, ub: bounds
 * muk: prox parameter
 * bundleManagerFun:
 * bundleManagerContext:
 * context: method context
 *
 * output:
 * =======
 * status: 0 if OK.
 * sa: aggregated subgradient
 * zkprox:  next point (solution of the quadratic model)
 * proxOptVal: optimal value of the quadratic model
 * deltak:
 * c0a: constant term of the aggregated line
 *
 */


	void SetDims_FORT(int *mt, int *n, int *memax, int *niz, int *ndz, int *iparam);
	
	void InitVecs_FORT(int *imp, double *fxn, double *errabs, int *iparam, double *param, int *iz, int *niz, double *dz, int *ndz);

	void BundleCall_FORT(int *n, int *memax, int *indic, double *fp, double *point, double *gp, double *binf, double *bsup, int *mode, int *nsim, int *iparam, double *param, int *iz, int *niz, double *dz, int *ndz);
#endif
