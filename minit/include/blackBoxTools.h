#ifndef BLACKBOXTOOLS_H
#define BLACKBOXTOOLS_H
#include<stdlib.h>

typedef int blackBox(const double *x, const int n,  const int mode, double *fval, double *g, void *context);
typedef blackBox *blackBoxP;
#endif
