#ifndef VECTOROPERATIONS_H 
#define VECTOROPERATIONS_H 
void vector_set_all(double * v, const double x, const int n);// all values are set to x
void vector_set_zero(double * v, const int n);
void vector_mencpy( double * dest, const double * src, const int n); 
void vector_add(double * a, const double * b, const int n);// a = a+b
void vector_sub(double * a, const double * b, const int n);// a=a-b
void vector_mul(double * a, const double * b, const int n);// a = a*b; termwise
void vector_scale(double * a, const double x, const int n);// a = x*a
void vector_add_constant(double * a, const double x, const int n);

double vector_inner_prod(const double *a, const double *b, int n);
double vector_inf_norm(const double *a, int n);

double vector_max(const double *a, int n, int *index);
double vector_add_all(const double * a, const int n);// =sum(a)
#endif
