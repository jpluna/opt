#include "vectorOperations.h"

void vector_set_all(double * v, const double x, const int n)
{
	int i;
	for (i = 0; i < n; i++)
	{
		v[i] = (double)x;
	};
};


void vector_set_zero(double * v, const int n)
{
	int i;
	for (i = 0; i < n; i++)
	{
		v[i] = 0;
	};
};

void vector_mencpy( double * dest, const double * src, const int n)
{
	int i;
	for (i = 0; i < n; i++)
	{
		dest[i] = src[i];
	};
};

void vector_add(double * a, const double * b, const int n)
{
	int i;
	for (i = 0; i < n; i++)
	{
		a[i] +=(double) b[i];
	};
};

void vector_sub(double * a, const double * b, const int n)
{
	int i;
	for (i = 0; i < n; i++)
	{
		a[i] -= b[i];
	};
};

void vector_mul(double * a, const double * b, const int n)
{
	int i;
	for (i = 0; i < n; i++)
	{
		a[i] *= b[i];
	};
};

void vector_scale(double * a, const double x, const int n)
{
	int i;
	for (i = 0; i < n; i++)
	{
		a[i] *= (double)x;
	};
};

void vector_add_constant(double * a, const double x, const int n)
{
	int i;
	for (i = 0; i < n; i++)
	{
		a[i] += (double)x;
	};
};

double vector_inner_prod(const double *a, const double *b, int n)
{
	double innerP = 0;
	int i;
	for (i=0; i<n; i++)
	{
		innerP += a[i] * b[i];
	};
	return innerP;
};


double vector_inf_norm(const double *a, int n)
{
	double norm;
	int i;
	norm = 0;
	for(i=0; i<n; i++)
	{
		if(a[i] > norm) 
		{ 
			norm = a[i];
		};
		if( (-1.0) * a[i] > norm) 
		{ 
			norm =  -a[i];
		};
	};
	return norm;
};


double vector_max(const double *a, int n, int *index)
{
	double max_val;
	int i;
	max_val = a[0];
	*index = 0;
	for(i=1; i<n; i++)
	{
		if(a[i] > max_val) 
		{ 
			max_val = a[i];
			*index = i;
		};
	};
	return max_val;
};

double vector_add_all(const double * a, const int n)
{
	double val = 0;
	int i;
	for(i=0;i<n;i++)
	{
		val += a[i];
	};
	return val;
};
