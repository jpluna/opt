# PyMinIt
This is a python library providing tools por minimizing functions. The package provides the following submodules:
- ## [tools](./pyminit/tools.md) 
- ## [unc](./pyminit/unc/README.md) Unconstrained Optimization.
- ## [ns](./pyminit/ns/README.md) Nonsmooth Optimization.

Install
=======
via pip from gitlab:

```python
pip install git+https://gitlab.com/jpluna/opt.git
```

Another option is to download the wheel [PyMinIt-0.0.1.dev2-py3-none-any.whl](./dist/PyMinIt-0.0.1.dev2-py3-none-any.whl) and install using pip

```python
pip install PyMinIt-0.0.1.dev2-py3-none-any.whl
```
